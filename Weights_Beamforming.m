%% Beamforming
% The aim of this script is to provide weights which can later be used for
% other functions such as that of holography.


%% Beamforming
Phase_array_all = zeros(Nant,Datapoints, Datapoints); % Allocate memory for speed
Src_dir_all     = zeros(Datapoints, Datapoints);

for i = 1:Datapoints
    
    Src_dir_all(i,:) = sqrt(l_size(i)^2 + l_size.^2); % source direction vector for all l and m
    
    for j = 1:Datapoints % check if not outside unit circle/ fov
        if Src_dir_all(i,j) > 1
            Src_dir_all(i,j) = 0;
        end
    end
end

for j = 1:Datapoints % for every l
    for k = 1:Datapoints % for every m

     % calc phase array vector
    Phase_array_all(:,j,k) = exp(-1j*2*pi*freqs(Fnum)/c*(Arraylayout*[l_size(j);l_size(k)]));

    end
end

Weights_BF = 1/Nant*Phase_array_all;


%% Minimum variance distortionless response

Weights_MVDR = zeros(Nant, Datapoints, Datapoints);

for i = 1:Datapoints
    for j = 1:Datapoints
        A_l = squeeze(Phase_array_all(:,i,j));
        Cov = squeeze(R(1:2:end, 1:2:end));
        Weights_MVDR(:,i,j) = (Cov/transpose(A_l))/(A_l'/Cov*A_l);
    end
end
