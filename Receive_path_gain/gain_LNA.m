function [outputArg1,outputArg2] = gain_LNA(inputArg1,inputArg2)
%GAIN_LNA Summary of this function goes here


%   Detailed explanation goes here

clear 
close all

% simulation parameters
Tavg       = 293;   % average temperature (K)

Tampl      = 10;    % amplitude of sinusoidal diurnal variations (K)
Lpeak      = 1000;  % maximum solar irradiance (W / m^2)
timescale  = 100;   % time scale of Gaussian process (s)
spatscale  = 35;    % spatial scale of Gaussian proces (m)
Lrms       = 50;    % RMS irradiance variation across station (W / m^2)
LFOrms     = 10;    % RMS irradiance variation across FO links (W / m^2)

% parameters of LNA
LNA_error  = 5;     % Given in percentage % 
dT_LNA     = 15;    % raise of temperature inside LNA (K)
alphaLNA   = 0.01;  % sensitivity to solar irradiation changes (K m^2 / W)
alphaLNA_e = 1e-3;  % RMS variation on alphaLNA (K m^2 / W)
eps_mag_LNA= 0.44;  % RMS gain error in dB
eps_ph_LNA = 2.9;   % RMS phase error in deg

% collect simulation parameters for storage along with results
params = who;
Nparams = length(params);
attr = cell(2 * (Nparams + 6), 1);
for n = 1:Nparams
    attr{2 * n - 1} = params{n};
    attr{2 * n} = double(eval(params{n}));
end

%frequency range (Hz)
f = (50:5:350).' * 1e6;
Nf = length(f);

%% Temperature and solar irradiance

% model for ambient temperature
% define time axis (s)
tstart = 0;         % start time, 0 means 0:00 h at night
tstop = 24 * 3600;  % stop time (excluded from time series)
Nt = 100;           % number of time slots to emulate
dt = (tstop - tstart) / Nt;
t = (0:Nt-1).' * dt;
% ambient temperature (K)
T0 = Tavg + Tampl * sin(pi + 2 * pi * (t / (24 * 3600)));
% solar irradiance (W / m^2)
L0 = max(0, Lpeak * sin(-pi/2 + 2 * pi * (t / (24 * 3600))));

% antenna positions
load Arraylayout_DiNinni_SKA-LOW_38m.mat
Nant = length(Arraylayout);

% add dimensions of simulations to attributes
attr{2 * Nparams + 1} = 'Nant';
attr{2 * Nparams + 2} = int32(Nant);
attr{2 * Nparams + 3} = 'Nf';
attr{2 * Nparams + 4} = int32(Nf);
attr{2 * Nparams + 5} = 'Nt';
attr{2 * Nparams + 6} = int32(Nt);

attr{2 * Nparams + 7} = 'dim_Nant';
attr{2 * Nparams + 8} = int32(1);
attr{2 * Nparams + 9} = 'dim_Nf';
attr{2 * Nparams + 10} = int32(2);
attr{2 * Nparams + 11} = 'dim_Nt';
attr{2 * Nparams + 12} = int32(3);

% 
phi = atan2(Arraylayout(:, 1), Arraylayout(:, 2));
[phi_sorted, sortidx] = sort(phi);  % sorted azimuthal angles
SBpos = zeros(Nant, 2);             % positions of SMARTBoxes in m
for idx = 1:16:Nant
    SBpos(sortidx(idx:idx+15), 1) = 15 * cos(mean(phi_sorted(idx:idx+15)));
    SBpos(sortidx(idx:idx+15), 2) = 15 * sin(mean(phi_sorted(idx:idx+15)));
end





end

