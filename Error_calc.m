%% Error_calc
%
% The function of this script is to see the errors that occur in each gain 
% stage, and see to what maximum extend the total deviation from the 
% average is at each gain stage, and what for the total gain. And how each
% individual component acts in terms of gain and phase versus the total
% signal chain as a function of time and frequency. 
%
% To get the interpretation of said data figures are made using
% Error_calc_figures.m.
% 
%% Run scripts for all variables neccesary
close all
run('./Receive_path_gain/receive_path_gain.m')

Nreg  = 5; % Receive path gain modules + 1
%% Sequence of hardware is LNA->FEM->FO->PREADU

%Allocate space LNA for max/min/mean amp/ph
max_lin_LNAx = zeros(Nf,Nt);
max_ph_LNAx  = zeros(Nf,Nt);
min_lin_LNAx = zeros(Nf,Nt);
min_ph_LNAx  = zeros(Nf,Nt);

mean_lin_LNAx= zeros(Nf,Nt);
mean_ph_LNAx = zeros(Nf,Nt);

% FEM

max_lin_FEMx = zeros(Nf,Nt);
max_ph_FEMx  = zeros(Nf,Nt);
min_lin_FEMx = zeros(Nf,Nt);
min_ph_FEMx  = zeros(Nf,Nt);

mean_lin_FEMx= zeros(Nf,Nt);
mean_ph_FEMx = zeros(Nf,Nt);

% FO

max_lin_FOx = zeros(Nf,Nt);
max_ph_FOx  = zeros(Nf,Nt);
min_lin_FOx = zeros(Nf,Nt);
min_ph_FOx  = zeros(Nf,Nt);

mean_lin_FOx= zeros(Nf,Nt);
mean_ph_FOx = zeros(Nf,Nt);

% preADU

max_lin_preADUx = zeros(Nf,Nt);
max_ph_preADUx  = zeros(Nf,Nt);
min_lin_preADUx = zeros(Nf,Nt);
min_ph_preADUx  = zeros(Nf,Nt);

mean_lin_preADUx= zeros(Nf,Nt);
mean_ph_preADUx = zeros(Nf,Nt);

% tot
max_lin_totx = zeros(Nf,Nt);
max_ph_totx  = zeros(Nf,Nt);
min_lin_totx = zeros(Nf,Nt);
min_ph_totx  = zeros(Nf,Nt);

mean_lin_totx= zeros(Nf,Nt);
mean_ph_totx = zeros(Nf,Nt);

% find max/min/mean for error in magnitude and phase of each antenna

for i = 1:Nt % for all timesteps
    for j = 1:Nf % for all frequencies
        
        %LNA
        max_lin_LNAx(j,i) = max(abs(gLNAx(:,j,i)));
        max_ph_LNAx(j,i)  = max(angle(gLNAx(:,j,i)));
        min_lin_LNAx(j,i) = min(abs(gLNAx(:,j,i)));
        min_ph_LNAx(j,i)  = min(angle(gLNAx(:,j,i)));
        mean_lin_LNAx(j,i) = mean(abs(gLNAx(:,j,i)));
        mean_ph_LNAx(j,i)  = mean(angle(gLNAx(:,j,i)));
        
        %FEM
        max_lin_FEMx(j,i) = max(abs(gFEMx(:,j,i)));
        max_ph_FEMx(j,i)  = max(angle(gFEMx(:,j,i)));
        min_lin_FEMx(j,i) = min(abs(gFEMx(:,j,i)));
        min_ph_FEMx(j,i)  = min(angle(gFEMx(:,j,i)));
        mean_lin_FEMx(j,i) = mean(abs(gFEMx(:,j,i)));
        mean_ph_FEMx(j,i)  = mean(angle(gFEMx(:,j,i)));
        
        %FO
        max_lin_FOx(j,i) = max(abs(gFOx(:,j,i)));
        max_ph_FOx(j,i)  = max(angle(gFOx(:,j,i)));
        min_lin_FOx(j,i) = min(abs(gFOx(:,j,i)));
        min_ph_FOx(j,i)  = min(angle(gFOx(:,j,i)));
        mean_lin_FOx(j,i) = mean(abs(gFOx(:,j,i)));
        mean_ph_FOx(j,i)  = mean(angle(gFOx(:,j,i)));
        
        %preADU
        max_lin_preADUx(j,i) = max(abs(g_preADUx(:,j,i)));
        max_ph_preADUx(j,i)  = max(angle(g_preADUx(:,j,i)));
        min_lin_preADUx(j,i) = min(abs(g_preADUx(:,j,i)));
        min_ph_preADUx(j,i)  = min(angle(g_preADUx(:,j,i)));
        mean_lin_preADUx(j,i) = mean(abs(g_preADUx(:,j,i)));
        mean_ph_preADUx(j,i)  = mean(angle(g_preADUx(:,j,i)));
        
        %total
        max_lin_totx(j,i) = max(abs(g_totx(:,j,i)));
        max_ph_totx(j,i)  = max(angle(g_totx(:,j,i)));
        min_lin_totx(j,i) = min(abs(g_totx(:,j,i)));
        min_ph_totx(j,i)  = min(angle(g_totx(:,j,i)));
        mean_lin_totx(j,i) = mean(abs(g_totx(:,j,i)));
        mean_ph_totx(j,i)  = mean(angle(g_totx(:,j,i)));
    end
end
%%  See max errors for timesteps of 5

%allocate space for max error, max gains, and min gains per stage and total
Max_error_stage = zeros(Nreg+1,Nf,Nt/5);
Max_gain        = zeros(1,Nreg);
Min_gain        = zeros(1,Nreg); 

for a = 1:Nt/5 % timestep of five min for 24hours
    Means_ant = zeros(Nant, Nf,Nreg); %allocate space and refresh for safety  
    for i = 1:Nant      % for each antenna
        for j = 1:Nf    % for each frequency
            % Find the mean in magnitude for each timestep of 5
            Means_ant(i,j,1) = mean(abs(gLNAx(i,j,((a-1)*5+1):5*a))); %LNA
            Means_ant(i,j,2) = mean(abs(gFEMx(i,j,((a-1)*5+1):5*a))); %FEM           
            Means_ant(i,j,3) = mean(abs(gFOx(i,j, ((a-1)*5+1):5*a))); %FO
            Means_ant(i,j,4) = mean(abs(g_preADUx(i,j,((a-1)*5+1):5*a)));
            
            Means_ant(i,j,5) = mean(abs(g_totx(i,j,((a-1)*5+1):5*a)));
        end
    end
    
    for i = 1:Nf % for each frequency
        for j = 1:Nreg % for each stage
            % find highest difference possible between mean i.e. max error
            error = (20*log10(max(abs(Means_ant(:,i,j))))-20*log10(min(abs(Means_ant(:,i,j)))))...
                   /(20*log10(mean(abs(Means_ant(:,i,j)))));
            
            % if error is larger, max error is smaller -> update 
            if Max_error_stage(j,i,a)<error
                Max_error_stage(j,i,a) = error;
            end
            % find values Max and Min for worst case scenario 
            Max_gain(j) = 20*log10(max(abs(Means_ant(:,i,j))));
            Min_gain(j) = 20*log10(min(abs(Means_ant(:,i,j))));
            
        end
        % worst case, the lowest value is the correct value, hence
        % diff/value = error.
        Max_error_stage(6,i,a) = (sum(Max_gain(1:4))-sum(Min_gain(1:4)))/sum(Min_gain(1:4));
    end
end

%% Get phases in a single usable matrix
Ph_mean = zeros(Nant, Nf, Nt, Nreg); % allocate space

for i = 1:Nant % for each antenna
    for j = 1:Nf % for each frequency
        for k = 1:Nt % for every fifth timestep
            % find phase mean over 5mins for each signal chain module
            Ph_mean(i,j,k,1) = (angle(gLNAx(i,j,k)));%(k-1)*5+1:5*k)));
            Ph_mean(i,j,k,2) = (angle(gFEMx(i,j,k)));%(k-1)*5+1:5*k)));
            Ph_mean(i,j,k,3) = (angle(gFOx( i,j,k)));%(k-1)*5+1:5*k)));
            Ph_mean(i,j,k,4) = (angle(g_preADUx(i,j,k)));%(k-1)*5+1:5*k)));
            
            Ph_mean(i,j,k,5) = (angle(g_totx(i,j,k)));%(k-1)*5+1:5*k)));
        end
    end
end

%% Phase error analysis

full_angle = zeros(Nant,Nf,Nt,Nreg);

% translate to full angle outside unit circle
full_angle(:,:,:,1) = unwrap(angle(gLNAx(:,:,:)));
full_angle(:,:,:,2) = unwrap(angle(gFEMx(:,:,:)));
full_angle(:,:,:,3) = unwrap(angle(gFOx(:,:,:)));
full_angle(:,:,:,4) = unwrap(angle(g_preADUx(:,:,:)));

full_angle(:,:,:,5) = unwrap(angle(g_totx(:,:,:)));

%%
% Allocate space for speed
full_angle_mean= zeros(Nf, Nt, Nreg);
full_angle_max = zeros(Nf, Nt, Nreg);
full_angle_min = zeros(Nf, Nt, Nreg);

full_angle_deg = full_angle/pi*180; % rad2deg

% find average/min/max over all antennas
for i = 1:Nf
    for j = 1:Nt
        for idx = 1:Nreg
            full_angle_mean(i,j,idx) = mean(full_angle_deg(:,i,j,idx)); % average of each antenna
            full_angle_min (i,j,idx) = min(full_angle_deg(:,i,j,idx));  % minimum of each antenna
            full_angle_max(i,j,idx)  = max(full_angle_deg(:,i,j,idx));  % maximum of each antenna
        end
    end
end

% allocate space for speed
max_phase_error = zeros(Nreg+1,Nf,Nt);

% find errors between phase/time and region
for i = 1:Nf % for each frequency
    for j = 1:Nt % for each 5mins
        for k = 1:Nreg % for each module + tot
            error1 = (full_angle_max(i,j,k) -full_angle_mean(i,j,k)); % difference in degree from max
            error2 = (full_angle_mean(i,j,k)-full_angle_min(i,j,k));  % difference in degree from min
            
            % find biggest angle difference and make it max error
            if max_phase_error(k,i,j)<error1
                max_phase_error(k,i,j) = error1;
            end
            if max_phase_error(k,i,j)<error2
                max_phase_error(k,i,j) = error2;
            end
        end
            % find max possible phase difference
            max_phase_error(6,i,j) = (sum(full_angle_max(i,j,1:4))-sum(full_angle_min(i,j,1:4)));
    end
end


%% Calculate the standard deviations for gain and phase
% allocate space nfor speed
Phase_std         = zeros(Nf,Nt,Nreg);
Gain_std          = zeros(Nf,Nt,Nreg);
Mean_phase_std    = zeros(Nf,Nt,Nreg);
Mean_gain_std     = zeros(Nf,Nt,Nreg);

for i = 1:Nf % for every frequency
    for j = 1:Nt % for every timestep
        for idx = 1:Nreg % for each module + tot
            Phase_std(i,j,idx)      = std(squeeze(full_angle_deg(:,i,j,idx))); % get phase standard deviation
            Mean_phase_std(i,j,idx) = mean(squeeze(full_angle_deg(:,i,j,idx)));% get mean for comparison 
        end
        % get gain standard deviations per module + tot
        Gain_std(i,j,1) = std(mag2db(abs(squeeze(gLNAx(:,i,j)))));
        Gain_std(i,j,2) = std(mag2db(abs(squeeze(gFEMx(:,i,j)))));
        Gain_std(i,j,3) = std(mag2db(abs(squeeze(gFOx(:,i,j)))));
        Gain_std(i,j,4) = std(mag2db(abs(squeeze(g_preADUx(:,i,j)))));
        Gain_std(i,j,5) = std(mag2db(abs(squeeze(g_totx(:,i,j)))));
        
        % get gain means per module + tot
        Mean_gain_std(i,j,1) = mean(mag2db(abs(squeeze(gLNAx(:,i,j)))));
        Mean_gain_std(i,j,2) = mean(mag2db(abs(squeeze(gFEMx(:,i,j)))));
        Mean_gain_std(i,j,3) = mean(mag2db(abs(squeeze(gFOx(:,i,j)))));
        Mean_gain_std(i,j,4) = mean(mag2db(abs(squeeze(g_preADUx(:,i,j)))));
        Mean_gain_std(i,j,5) = mean(mag2db(abs(squeeze(g_totx(:,i,j)))));
    end
end

