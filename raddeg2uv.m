function [Output_array] = raddeg2uv(u, v, input_array, l, m)
%RADDEG2UV Converts the array which is currently present as the index from
%theta to phi towards one that is present in u v coordinates. The
%input_array are values following the theta and phi indexing similar to l
%and m. The result is a 2D array following the u v coordinates. 
%
% Note that u and v here are the l and m coordinates, just in a different 

%   Detailed explanation goes here
Output_array = zeros(length(u),length(v));

for i = 1:length(input_array)
        
    if length(input_array)<length(l) 
        
        index = input_array(i,1);
        [~, index_u] = min(abs(u' - l(index)));
        [~, index_v] = min(abs(v' - m(index)));
        
        Output_array(index_u, index_v) = real(input_array(i,2));
        
    else
        [~, index_u] = min(abs(u' - l(i)));
        [~, index_v] = min(abs(v' - m(i)));
  
        Output_array(index_u, index_v) = real(input_array(i));
    end

end

