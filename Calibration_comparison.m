%% Calibration comparison. 
% This idea of this script is to compare the functionality/behaviour of
% self-calibration and holography. The source model used is generated from
% Cov_Matrix_gen.m.

% % Calibration is currently for a single frequency and single time interval
% clear all % just to be sure
% clc       % erases all chat in command window
%% Variables
Fnum  = 13;   % Frequency index corresponding to 110MHz
err   = 1e-4; % max difference between iterations
imax  = 300 ;  % max number of iterations
Time  = 1;    % minute of measurement 1:1440
MVDR  = 0;    % set 1 if MVDR as a weight is used
Ran   = 1;    % set to 0 if need to load values and generate covariance matrices
Ndata = 201;  % amount of datapoints in lm coordinates
src   = 11;   % source of Signus A for holography
%% Required scriptsMap_EEP_holo
if Ran == 0
run('./Receive_path_gain/receive_path_gain.m') % Provides Gains of the receive path
disp('Done with receive path gain')
run('./Cov_Matrix_gen.m')                      % Provides Covariance matrix 
disp('Done with Cov_Matrix_gen')
run('./Weights_Beamforming.m')                 % Provides Weights for Holography (BF and MVDR)
disp('Done with Weights_Beamforming')
end

%% Calibration methods

% get single source covariance matrix
Sig = pol_EEP_SKALA4AL(lm(src,1), lm(src,2), x_pos, y_pos, f(Fnum));
for j = 1:Nant % for each antenna
    
    Phase_Gain_vec(2*j-1:2*j,:) = squeeze(Sig(:,:,j,1)); % get phasevector * gain vector per antenna
end
R_single = (Phase_Gain_vec*sigma_src(src)*Phase_Gain_vec'); % get covarience

% Rewrite for visibility and normalize to first antenna 
R_model = squeeze(R(:,:));               % Get model covariance matrix
% R_model = R_model/(abs(R_model(1,1)));     % Normalize R_model 

% allocate memory for speed
G_chain = zeros(2*Nant,1);
% get gains of the signal chain
for i = 1:Nant % for each antenna
G_chain(i*2-1) = squeeze(g_totx(i,Fnum,Time)); % signal chain gain x polarization are all odd number
G_chain(i*2)   = squeeze(g_toty(i,Fnum,Time)); % signal chain gain y polarization even numbers
end

R_meas  = diag(G_chain)*R_model*diag(G_chain)'; % Calc measured covariance matrix
R_meas  = R_meas/(abs(R_meas(1,1)));            % Normalize R_measured

norm_2 = abs(G_chain(1)*R(1,1)*G_chain(1)');    % get normalization factor

% Sigma_m = diag(G_chain(1:2:end))*Sigma*diag(G_chain(1:2:end))'; % Calc Signal covariance matrix
% Sigma_m = Sigma_m/max(abs(Sigma_m(1,1)));         % Normalzie Sigma_measured

skymap        = acm2skyimage(R(1:2:end, 1:2:end), x_pos, y_pos, f(Fnum), l_size, l_size);
[~, max_holo] = max(skymap(:));
[idx1, idx2]  = ind2sub([Ndata,Ndata],max_holo);
Weight        = squeeze(Weights_BF(1,:, idx1, idx2)).';

% Perform calibration methods
[G_self, i_self]                   = Self_calibration(R_model, R_meas, err, imax);
% [G_stef, i_stef]                   = stefcal(R_model, R_meas, err, imax);
[G_stef, i_stef]                   = gainsolv(err,R_model, R_meas, ones(2*Nant,1),imax);
[G_holo, i_holo]                   = Holography(R_meas(1:2:end,1:2:end), Weight, err, imax );

%% Apply gains after calibration
g_self_new                = squeeze(G_self(:,i_self));
g_stef_new                = squeeze(G_stef(:,i_stef-1));
g_holo_new                = G_holo(:, i_holo+1);

R_holo         = diag(1./g_holo_new)*R_meas(1:2:end, 1:2:end)*diag(1./g_holo_new)';
skymap_holo    = acm2skyimage(R_holo, x_pos, y_pos, f(Fnum), l_size, l_size);
[~, maxidx]    = max(skymap_holo(:));
[idx1a, idx2a] = ind2sub([Ndata, Ndata], maxidx);

w1             = squeeze(Weights_BF(1, :, idx1a, idx2a)).'; % weights to point in the direction of maximum (geometrical delays)
g_holo_new     = g_holo_new .* (conj(w1) .* Weight); % apply pointing correction

% make phase of first element equal to phase of first element obtained by
% Self_Calibration to facilitate comparison
g_holo_new     = g_holo_new * exp(-1i * (angle(g_holo_new(1)) - angle(g_self_new(1))));

% update covariance matrix for each calibration method
R_self = diag(g_self_new)*R_meas*diag(g_self_new)';
R_stef = diag(conj(g_stef_new))*R_meas*diag(conj(g_stef_new))';
R_holo = diag(1./g_holo_new)*R_meas(1:2:end, 1:2:end)*diag(1./g_holo_new)';


%% Visualization by use of acm2skyimage

Uncalibrated_map = acm2skyimage(R(1:2:end,1:2:end)/abs(R(1,1)), x_pos, y_pos, f(Fnum), l_size, l_size);
Map_self         = acm2skyimage(R_self(1:2:end,1:2:end)       , x_pos, y_pos, f(Fnum), l_size, l_size);
Map_holo         = acm2skyimage((R_holo)                  , x_pos, y_pos, f(Fnum), l_size, l_size);
Map_stef         = acm2skyimage(R_stef(1:2:end,1:2:end)       , x_pos, y_pos, f(Fnum), l_size, l_size);


%% test
g_holo_test = G_holo(:,i_holo+1).*(conj(Weight));
g_holo_test = g_holo_test * exp(-1i*(angle(g_self_new(1))));
R_holo_test = diag(g_holo_test)*R_meas(1:2:end, 1:2:end)*diag(g_holo_test)';

test = acm2skyimage(R_holo_test, x_pos, y_pos, f(Fnum), l_size, l_size);
% try to find single source -> version now in Cov_Matrix_gen.m
% Rhat = diag(1./G_holo(:,i_holo-1))*R_meas(1:2:end, 1:2:end)*diag(1./G_holo(:,i_holo-1))';
% A = exp(-(2*pi*-1j*f(Fnum)/c)*(Arraylayout(:,1:2)*[l_size(idx1a),l_size(idx2a)].'));
% Sigma_c = real(((abs(A' *A).^ 2)\Khatri_Rao(conj(A), A)'*(Rhat(:))));



g_test2 = G_holo(:,i_holo).*exp(-1i*angle(A));

test2 = acm2skyimage(diag(1./G_holo(:,i_holo))*R_meas(1:2:end, 1:2:end)*diag(1./G_holo(:,i_holo))',x_pos, y_pos, f(Fnum), l_size, l_size); 


figure
subplot(1,4,1)
imagesc(Map_holo)
colorbar
subplot(1,4,2)
imagesc(test)
colorbar
subplot(1,4,3)
imagesc(test2)
colorbar
subplot(1,4,4)
imagesc(Map_stef)
colorbar

figure
subplot(1,2,1)
plot(1:Nant, -angle(g_stef_new(1:2:end)), 'mx', ...
     1:Nant, angle(g_holo_new), 'g+')%,    ... 
%      1:Nant, angle(g_holo_test), 'bo', ...
%      1:Nant, angle(g_test2), 'r.');
%   
legend('Gain stef ','Gain Holo')%, 'Gain holo test', 'test2'
title('phase results')
subplot(1,2,2)
plot(1:Nant, abs(g_holo_test), 'bo', 1:Nant, abs(g_stef_new(1:2:end)), 'mx');%,1:Nant, abs(g_test2),'r.', 1:Nant, abs(G_holo(:,i_holo)), 'g+' );
legend('Gain holo corrected', 'Gain Stef')%,'Gain test2', 'Gain Holo')
title('gain results')

%%
figure
plot(1:Nant, -angle(g_stef_new(1:2:end)), 'bo',...
     1:Nant, -angle(1./g_holo_new)- 2*angle(g_self_new(1)), 'mx',...
     1:Nant, angle(1./G_chain(1:2:end))-pi/2, 'c.')
legend('Gain Stef', 'Gain Holo', 'Gain Ideal')
grid on
axis([0 256, -inf inf])
xlabel('Antenna nr [-]')
ylabel('Phase [rad]')
%%

norm_weight = abs(Weight(:));
figure
plot(1:Nant, abs(1./g_stef_new(1:2:end)/sqrt(norm_2)), 'bo',...
     1:Nant, abs(g_holo_new./(norm_weight*1e3)), 'mx')%,...
     %1:Nant, abs(1./G_chain(1:2:end)), 'c.')
legend('Gain Stef', 'Gain Holo', 'Gain Ideal')
grid on
axis([0 256, -inf inf])
xlabel('Antenna nr [-]')
ylabel('Magnitude gain')
 
%% Check holography for different sources

% Allocate memory for speed
%Gain       = zeros(Nant*2,1);             % Variable for multiplication after receiving gain from holography
R_holo_srcs= zeros(Nant, Nant, Nsrc); % Calibrated covarience matrix for each source.

for i = 1:Nsrc % for each source
    W_iters                = squeeze(Weights_BF(1,:,Source_idx(i,1), Source_idx(i,2))).';
    [G_holo_s, i_holo_s]   = Holography(R_meas(1:2:end,1:2:end), W_iters ,err, imax); % perform holography
    
    G_holo_s               = G_holo_s(:,i_holo_s);
    
    % make phase of first element equal to phase of first element obtained by
    % Self_Calibration to facilitate comparison
    G_holo_s     = G_holo_s * exp(-1i * (angle(G_holo_s(1)) - angle(g_self_new(1))));
          
    R_holo_srcs(:,:,i) = diag(1./G_holo_s)*R_meas(1:2:end, 1:2:end)*diag(1./G_holo_s)';                                     % Calc cov matrices
   
end

% imaging using acm2skyimage
Holo_maps =mag2db( acm2skyimage(R_holo_srcs, x_pos, y_pos, f(Fnum), l_size, l_size));

%% Gain investigation
g_test = squeeze(g_totx(:,Fnum,Time));
g_ideal = 1./G_chain;

figure
subplot(1,2,1)
plot(1:256, angle(g_self_new(1:2:end)), 'bo', ...
    1:256, angle(g_stef_new(1:2:end)), 'mx', ...
     1:256, angle(g_holo_new), 'g+',...
     1:Nant, angle(g_ideal(1:2:end)), 'c*')
   % 1:256, angle(G_gainsolv(1:2:end)), 'rd', ...
legend('Gain Self', 'Gain Stef', 'Gain Holo', 'Ideal Gain')
subplot(1,2,2)
plot(1:Nant, abs(g_self_new(1:2:end)), 'bo', 1:Nant, abs(g_stef_new(1:2:end)), 'mx'...
    ,1:Nant, abs(G_holo(:,i_holo)), 'g+' , 1:Nant, abs(g_ideal(1:2:end)), 'c*');
legend('Gain Self', 'Gain Stef', 'Gain Holo', 'Ideal Gain')




%% Figures go here
% get all sky images and confined
Uncalibrated_map = (Confine(l_size, l_size, Ndata, Uncalibrated_map));
Map_self         = (Confine(l_size, l_size, Ndata,Map_self));
Map_holo         = (Confine(l_size, l_size, Ndata,Map_holo));
Map_stef         = (Confine(l_size, l_size, Ndata,Map_stef));

figure
subplot(2,2,1)
imagesc(Uncalibrated_map./max(max(Uncalibrated_map)))
title('Uncalibrated map from R')
colorbar
subplot(2,2,2)
imagesc(Map_self./max(max(Map_self)))
title('Calibrated map from R\_self')
colorbar
subplot(2,2,3)
imagesc(Map_holo./max(max(Map_holo)))
title('Calibrated map from R\_holo')
colorbar
subplot(2,2,4)
imagesc(Map_stef./max(max(Map_stef)))
title('Calibrated map from R\_stef')
colorbar

% get holography result for each source
figure
subplot(3,4,1)
imagesc(squeeze(Holo_maps(:,:,1)))
title(['Holo Source nr' num2str(1)])
colorbar
subplot(3,4,2)
imagesc(squeeze(Holo_maps(:,:,2)))
title(['Holo Source nr' num2str(2)])
colorbar
subplot(3,4,3)
imagesc(squeeze(Holo_maps(:,:,3)))
title(['Holo Source nr' num2str(3)])
colorbar
subplot(3,4,4)
imagesc(squeeze(Holo_maps(:,:,4)))
title(['Holo Source nr' num2str(4)])
colorbar
subplot(3,4,5)
imagesc(squeeze(Holo_maps(:,:,5)))
title(['Holo Source nr' num2str(5)])
colorbar
subplot(3,4,6)
imagesc(squeeze(Holo_maps(:,:,6)))
title(['Holo Source nr' num2str(6)])
colorbar
subplot(3,4,7)
imagesc(squeeze(Holo_maps(:,:,7)))
title(['Holo Source nr' num2str(7)])
colorbar
subplot(3,4,8)
imagesc(squeeze(Holo_maps(:,:,8)))
title(['Holo Source nr' num2str(8)])
colorbar
subplot(3,4,9)
imagesc(squeeze(Holo_maps(:,:,9)))
title(['Holo Source nr' num2str(9)])
colorbar
subplot(3,4,10)
imagesc(squeeze(Holo_maps(:,:,10)))
title(['Holo Source nr' num2str(10)])
colorbar
subplot(3,4,11)
imagesc(squeeze(Holo_maps(:,:,11)))
title(['Holo Source nr' num2str(11)])
colorbar
subplot(3,4,12)
imagesc(squeeze(Holo_maps(:,:,12)))
title(['Holo Source nr' num2str(12)])
colorbar

%% figure for gain convergence progress

figure
for i = 1:i_holo
    plot(real(G_holo(1,i)), imag(G_holo(1,i)), 'g+')
    text(real(G_holo(1,i)), imag(G_holo(1,i)), num2str(i))
    hold on 
end

figure
for i = 1:i_stef
    plot(real(G_stef(1,i)), imag(G_stef(1,i)), 'm.')
    text(real(G_stef(1,i)), imag(G_stef(1,i)), num2str(i))
    hold on 
end

%% 
figure
P = pcolor(l_size,l_size,(Map_holo./(max(max(Map_holo)))));
title('SKA skymap calibrated');
xlabel('East \leftarrow l \rightarrow West');
ylabel('South \leftarrow m \rightarrow North');
set(gca,'YDir', 'normal', 'XDir', 'reverse');
set(P, 'EdgeColor', 'none');
colormap(turbo);
colorbar;
daspect([1 1 1]);

figure
subplot(1,2,1)
plot(angle(G_holo(:,1:i_holo-1)))
title('Gain phases of all antennas vs iteration')
xlabel('Iteration')
ylabel('Gain phase (rad)')
subplot(1,2,2)
plot(abs(G_holo(:,1:i_holo-1)))
title('Gain magnitude of all antennas vs iteration')
%% See if covariance is correct or similar
% 
% figure(8)
% subplot(2,2,1)
% imagesc(real(squeeze(R(:,:))))
% colorbar
% subplot(2,2,2)
% imagesc(real(R_model))
% colorbar
% subplot(2,2,3)
% imagesc(real(R_holo))
% colorbar
% subplot(2,2,4)
% imagesc(real(R_stef))
% colorbar



%% Quick figure to see if point is correct. 
Fov    = zeros(Ndata, Ndata);
Fov_op = zeros(Ndata, Ndata);

for i = 1:size(Source,1) % flip source positions --- check exponentials for sign changes
    Fov(Source_idx(i,1) ,Source_idx(i,2))            = 1; % sigma_src(i);
end

figure
subplot(1,2,1)
imagesc(Fov)
title('From Source')
subplot(1,2,2)
imagesc(mag2db(Uncalibrated_map))
title('Sky map before calibration [v]')
%%
figure
subplot(2,2,1)
imagesc(mag2db(Uncalibrated_map)./max(max(mag2db(Uncalibrated_map))))
title('Uncalibrated map from R')
colorbar
subplot(2,2,2)
imagesc(mag2db(Map_self)./max(max(mag2db(Map_self))))
title('Calibrated map from R\_self')
colorbar
subplot(2,2,3)
imagesc(mag2db(Map_holo)./max(max(mag2db(Map_holo))))
title('Calibrated map from R\_holo')
colorbar
subplot(2,2,4)
imagesc(mag2db(Map_stef)./max(max(mag2db(Map_stef))))
title('Calibrated map from R\_stef')
colorbar


%% function to get the actual visibilities
function EEP = Confine(u,v, datapoints, EEP)

        for i = 1:datapoints 
            for j = 1:datapoints
                if sqrt(u(i)^2 + v(j)^2)>1
                    EEP(i,j) = NaN;
                end
            end
        end
end