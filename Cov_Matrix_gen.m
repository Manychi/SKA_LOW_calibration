%% Script to generate the Covariance matrix
%
% This script sets up all the variables used also during calibration. It
% first loads up the array layout, after which it sets up the phase vectors
% for each frequency. 


%% Run neccesary files/loads
 load ./mat_files/Arraylayout_DiNinni_SKA-LOW_38m.mat % loads Arraylayout
 load ./mat_files/EEP_lm_SKALA4AL_110MHz_38m.mat      % loads complex gains
 
 load ./mat_files/SHsource_data.mat                   % loads sources with phi/theta and flux
 
%% Variables

Nant    = size(Ephi_lm,2);  % total number of antennas in a single station
Npol    = size(Ephi_lm,1);  % number of polarizations of the signal received

% freqs   = (50:5:350)*1e6; % all frequencies [Hz] to be calibrated for
freqs   = 110*1e6;        % frequencies of interest / kept this way to allow use of more frequencies
c       = 3e8;            % speed of light / rough approximation

InputPw = 0.0025;         % Input power of each antenna
Nf      = size(freqs,1);  % Number of frequencies 

% Ndata = 101;         % amount of points in u and v / l and m

x_pos            = Arraylayout(:,1); % Get antenna positions in x direction
y_pos            = Arraylayout(:,2); % Get antenna positions in y direction

l_size           = -1:2/(Ndata-1):1; % set up size for direction cosine lm
[l_grid, m_grid] = meshgrid(l_size);      % set up grid for l and m

rasrc      = SHsources.ra ;  % get azimuth angle of each source
decsrc     = SHsources.dec;  % get zenith  angle of each source
flux70MHz  = SHsources.flux70MHz;  % load flux intensity from 70MHz
flux450MHz = SHsources.flux450MHz; % load flux intensity from 450MHz

lon    = 116.7 * (pi / 180);           % longitude of AAVS site in rad
lat    = -26.7 * (pi / 180);           % latitude of AAVS site in rad
tobs   = timestamp(Time); % take a moment of measurement

% determine where the sources are in the local horizon system
[lsrc, msrc] = radectolm(rasrc, decsrc, JulianDay(tobs), lon * 180 / pi, lat * 180 / pi);

srcsel       = ~isnan(lsrc);                 % if position is NaN, the source is below local horizon plane
lm           = [lsrc(srcsel), msrc(srcsel)]; % get lm for all visible points
Nsrc         = size(lm,1);                   % get all visibilities

%% Source model 
% Source_idx  = [30, 20; 40, 40]; % get index of each source position
% Src_num     = (Source_idx(:,1)-1)*Ndata + Source_idx(:,2); % get lm cascaded l and m
% 
% Source      = [l_size(Source_idx(:,1)); l_size(Source_idx(:,2))]; % Position of points in [l,m]
% Signal_Cov  = 20*ones(size(Source,1));  % Intensity of signal sources
% Noise_Cov   = eye(Nant);                % Intensity of noise sources
% 
% l_q_src = [(Source(:,1)), (Source(:,2))]; % source direction vector
% a_q_src = zeros(Nf, Nant);                % allocate memory for speed
% 
% for i = 1:Nf % for every frequency
% a_q_src(i,:) = exp(-1j*2*pi*f/c*(Arraylayout*l_q_src')); % Calc Array response vector
% end


% Allocate memory for speed
Source      = zeros(Nsrc, 2);
Src_num     = zeros(Nsrc,1) ;
Source_idx  = zeros(Nsrc,2);

for i = 1:Nsrc % for each source
    [~, l_idx] = min(abs(l_size - lm(i,1))); % find nearest index in l
    [~, m_idx] = min(abs(l_size - lm(i,2))); % find nearest index in m
    
    if sqrt(l_size(l_idx)^2 + l_size(m_idx)^2)>=1
       m_idx = m_idx -1;
       if sqrt(l_size(l_idx)^2 + l_size(m_idx)^2)>=1
           l_idx = l_idx -1;
       end
       disp('Casualty averted')
    end
    
    % issue with index getting outside of fov
    Source(i,:)     =  [l_size(l_idx), l_size(m_idx) ]; % get coordinates for each source
    Src_num(i)      =  (l_idx-1)*Ndata+m_idx; 
    Source_idx(i,:) =  [l_idx, m_idx];
end

a_q_src = exp(-1j*2*pi*f(Fnum)/c*(Arraylayout*Source')); % calc array response vector


%% Calculate Jones beam & Sigma_src
%gain         = ones(2*Nant); % fill in initial gains as ones
%J_elem_freqs = zeros(Nf, Npol, Npol, Nant, Ndata*Ndata);

%[~, J_elem] = SKA_LOW_statbeam(lm(:,1), lm(:,2), f, gain, l_size, l_size, x_pos, y_pos);
J_elem      = pol_EEP_SKALA4AL(lm(:,1), lm(:,2), x_pos, y_pos, f(Fnum));

% estimate flux at observing frequency assuming exponential power spectrum
flux70MHz  = flux70MHz(srcsel);    % get intensity in horizon plane
flux450MHz = flux450MHz(srcsel);   % get intensity in horizon plane


logflux = zeros(Nsrc, Nf); % allocate memory if multiple sources used
%%
freq_tot = (50:5:350)*1e6;  % all frequencies for which SKA_low is operational
Nft      = size(freq_tot,2);
for i = 1:Nft % for each frequency
    for j = 1:Nsrc % for each source
    logflux(j,i) = interp1([70e6, 450e6], log10([flux70MHz(j), flux450MHz(j)]), freq_tot(i), 'linear'); % estimate flux for each source
    end
end

sigma_src = 10.^logflux(:,Fnum);  % Source covariance matrix

%% Calculate Covarience matrix

%Allocate memory for speed
Phase_Gain_vec  = zeros(2*Nant,Npol);
R               = zeros(2 * Nant, 2 * Nant);

%for fidx = 1:Nf % for each frequency
    for i = 1:Nsrc % for each source
        for j = 1:Nant % for each antenna
            
            Phase_Gain_vec(2*j-1:2*j,:) = squeeze(J_elem(:,:,j,i)); % get phasevector * gain vector per antenna
        end
        
         R(:,:) = squeeze(R(:,:)) + (Phase_Gain_vec*sigma_src(i)*Phase_Gain_vec'); % get covarience
    end
%end

%% Signal Covarience matrix

%Calculate the signal covarience matrix
Sigma = zeros(Nant, Nant);
for i = 1:Nsrc
Sigma = Sigma + conj(a_q_src)*sigma_src(i)*conj(a_q_src)' ;
end
