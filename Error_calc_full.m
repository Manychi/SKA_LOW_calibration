%% Error_calc
%
% The function of this script is twofold, on one end it aims to see the
% errors that occur in each gain stage, and see to what maximum extend the
% total deviation from the average is at each gain stage, and what for the
% total gain. After that it is checked how each calibration method performs
% 
%% Run scripts for all variables neccesary
close all
run('./Receive_path_gain/receive_path_gain.m')

Nreg  = 5; % Receive path gain modules + 1
%% Sequence of hardware is LNA->FEM->FO->PREADU

%Allocate space LNA for max/min/mean amp/ph
max_lin_LNAx = zeros(Nf,Nt);
max_ph_LNAx  = zeros(Nf,Nt);
min_lin_LNAx = zeros(Nf,Nt);
min_ph_LNAx  = zeros(Nf,Nt);

mean_lin_LNAx= zeros(Nf,Nt);
mean_ph_LNAx = zeros(Nf,Nt);

% FEM

max_lin_FEMx = zeros(Nf,Nt);
max_ph_FEMx  = zeros(Nf,Nt);
min_lin_FEMx = zeros(Nf,Nt);
min_ph_FEMx  = zeros(Nf,Nt);

mean_lin_FEMx= zeros(Nf,Nt);
mean_ph_FEMx = zeros(Nf,Nt);

% FO

max_lin_FOx = zeros(Nf,Nt);
max_ph_FOx  = zeros(Nf,Nt);
min_lin_FOx = zeros(Nf,Nt);
min_ph_FOx  = zeros(Nf,Nt);

mean_lin_FOx= zeros(Nf,Nt);
mean_ph_FOx = zeros(Nf,Nt);

% preADU

max_lin_preADUx = zeros(Nf,Nt);
max_ph_preADUx  = zeros(Nf,Nt);
min_lin_preADUx = zeros(Nf,Nt);
min_ph_preADUx  = zeros(Nf,Nt);

mean_lin_preADUx= zeros(Nf,Nt);
mean_ph_preADUx = zeros(Nf,Nt);

% tot
max_lin_totx = zeros(Nf,Nt);
max_ph_totx  = zeros(Nf,Nt);
min_lin_totx = zeros(Nf,Nt);
min_ph_totx  = zeros(Nf,Nt);

mean_lin_totx= zeros(Nf,Nt);
mean_ph_totx = zeros(Nf,Nt);

% find max/min/mean for error in magnitude and phase of each antenna

for i = 1:Nt % for all timesteps
    for j = 1:Nf % for all frequencies
        
        %LNA
        max_lin_LNAx(j,i) = max(abs(gLNAx(:,j,i)));
        max_ph_LNAx(j,i)  = max(angle(gLNAx(:,j,i)));
        min_lin_LNAx(j,i) = min(abs(gLNAx(:,j,i)));
        min_ph_LNAx(j,i)  = min(angle(gLNAx(:,j,i)));
        mean_lin_LNAx(j,i) = mean(abs(gLNAx(:,j,i)));
        mean_ph_LNAx(j,i)  = mean(angle(gLNAx(:,j,i)));
        
        %FEM
        max_lin_FEMx(j,i) = max(abs(gFEMx(:,j,i)));
        max_ph_FEMx(j,i)  = max(angle(gFEMx(:,j,i)));
        min_lin_FEMx(j,i) = min(abs(gFEMx(:,j,i)));
        min_ph_FEMx(j,i)  = min(angle(gFEMx(:,j,i)));
        mean_lin_FEMx(j,i) = mean(abs(gFEMx(:,j,i)));
        mean_ph_FEMx(j,i)  = mean(angle(gFEMx(:,j,i)));
        
        %FO
        max_lin_FOx(j,i) = max(abs(gFOx(:,j,i)));
        max_ph_FOx(j,i)  = max(angle(gFOx(:,j,i)));
        min_lin_FOx(j,i) = min(abs(gFOx(:,j,i)));
        min_ph_FOx(j,i)  = min(angle(gFOx(:,j,i)));
        mean_lin_FOx(j,i) = mean(abs(gFOx(:,j,i)));
        mean_ph_FOx(j,i)  = mean(angle(gFOx(:,j,i)));
        
        %preADU
        max_lin_preADUx(j,i) = max(abs(g_preADUx(:,j,i)));
        max_ph_preADUx(j,i)  = max(angle(g_preADUx(:,j,i)));
        min_lin_preADUx(j,i) = min(abs(g_preADUx(:,j,i)));
        min_ph_preADUx(j,i)  = min(angle(g_preADUx(:,j,i)));
        mean_lin_preADUx(j,i) = mean(abs(g_preADUx(:,j,i)));
        mean_ph_preADUx(j,i)  = mean(angle(g_preADUx(:,j,i)));
        
        %total
        max_lin_totx(j,i) = max(abs(g_totx(:,j,i)));
        max_ph_totx(j,i)  = max(angle(g_totx(:,j,i)));
        min_lin_totx(j,i) = min(abs(g_totx(:,j,i)));
        min_ph_totx(j,i)  = min(angle(g_totx(:,j,i)));
        mean_lin_totx(j,i) = mean(abs(g_totx(:,j,i)));
        mean_ph_totx(j,i)  = mean(angle(g_totx(:,j,i)));
    end
end
%%  See max errors for timesteps of 5

%allocate space for max error, max gains, and min gains per stage and total
Max_error_stage = zeros(Nreg+1,Nf,Nt/5);
Max_gain = zeros(1,Nreg); Min_gain = zeros(1,Nreg); 

for a = 1:Nt/5 % timestep of five min for 24hours
    Means_ant = zeros(Nant, Nf,Nreg); %allocate space and refresh for safety  
    for i = 1:Nant      % for each antenna
        for j = 1:Nf    % for each frequency
            % Find the mean in magnitude for each timestep of 5
            Means_ant(i,j,1) = mean(abs(gLNAx(i,j,((a-1)*5+1):5*a))); %LNA
            Means_ant(i,j,2) = mean(abs(gFEMx(i,j,((a-1)*5+1):5*a))); %FEM           
            Means_ant(i,j,3) = mean(abs(gFOx(i,j, ((a-1)*5+1):5*a))); %FO
            Means_ant(i,j,4) = mean(abs(g_preADUx(i,j,((a-1)*5+1):5*a)));
            
            Means_ant(i,j,5) = mean(abs(g_totx(i,j,((a-1)*5+1):5*a)));
        end
    end
    
    for i = 1:Nf % for each frequency
        for j = 1:Nreg % for each stage
            % find highest difference possible between mean i.e. error
            error = (20*log10(max(abs(Means_ant(:,i,j))))-20*log10(min(abs(Means_ant(:,i,j)))))...
                   /(20*log10(mean(abs(Means_ant(:,i,j)))));
            
            % if error is larger, max error is smaller -> update 
            if Max_error_stage(j,i,a)<error
                Max_error_stage(j,i,a) = error;
            end
            % find values Max and Min for worst case scenario 
            Max_gain(j) = 20*log10(max(abs(Means_ant(:,i,j))));
            Min_gain(j) = 20*log10(min(abs(Means_ant(:,i,j))));
            
        end
        % worst case, the lowest value is the correct value, hence
        % diff/value = error.
        Max_error_stage(6,i,a) = (sum(Max_gain(1:4))-sum(Min_gain(1:4)))/sum(Min_gain(1:4));
    end
end

%% See phase changes per 5 mins
Ph_mean = zeros(Nant, Nf, Nt, Nreg); % allocate space

for i = 1:Nant % for each antenna
    for j = 1:Nf % for each frequency
        for k = 1:Nt % for every fifth timestep
            % find phase mean over 5mins for each signal chain module
            Ph_mean(i,j,k,1) = (angle(gLNAx(i,j,k)));%(k-1)*5+1:5*k)));
            Ph_mean(i,j,k,2) = (angle(gFEMx(i,j,k)));%(k-1)*5+1:5*k)));
            Ph_mean(i,j,k,3) = (angle(gFOx( i,j,k)));%(k-1)*5+1:5*k)));
            Ph_mean(i,j,k,4) = (angle(g_preADUx(i,j,k)));%(k-1)*5+1:5*k)));
            
            Ph_mean(i,j,k,5) = (angle(g_totx(i,j,k)));%(k-1)*5+1:5*k)));
        end
    end
end

%% Phase error analysis
full_angle = zeros(Nant, Nf, Nt, Nreg);
% translate to full angle outside unit circle
for i = 1:Nant % for each antenna
    for j = 1:Nf % for each frequency
        for k = 1:Nt % for each timestep
            full_angle(i,j,k,1) = full_angle(i,j,k,1) + angle(gLNAx(i,j,k)); % add angle to value
            full_angle(i,j,k,2) = full_angle(i,j,k,2) + angle(gFEMx(i,j,k));
            full_angle(i,j,k,3) = full_angle(i,j,k,3) + angle(gFOx(i,j,k));
            full_angle(i,j,k,4) = full_angle(i,j,k,4) + angle(g_preADUx(i,j,k));
            
            full_angle(i,j,k,5) = full_angle(i,j,k,5) + angle(g_totx(i,j,k));
            
            if k > 2 % after step 2 check the difference
                for idx = 1:Nreg % for each region
                    if full_angle(i,j,k, idx)  - full_angle(i,j,k-1,idx) > pi
                        full_angle(i,j,k:Nt, idx) = full_angle(i,j,k:Nt, idx) - 2*pi; % continue negative/ going downwards
                    end
                    if full_angle(i,j,k-1,idx) - full_angle(i,j,k,idx)   > pi
                        full_angle(i,j,k:Nt, idx) = full_angle(i,j,k:Nt, idx) + 2*pi; % continue positive/ going upwards
                    end
                end
            end 
        end
    end
end

% Allocate space for speed
%full_angle_avg = zeros(Nant, Nf, Nt/5, Nreg);
full_angle_mean= zeros(Nf, Nt, Nreg);
full_angle_max = zeros(Nf, Nt, Nreg);
full_angle_min = zeros(Nf, Nt, Nreg);
% 
% % find average over 5 mins to reduce noise
% for j = 1:Nf % for each frequency
%     for i = 1:Nant % for each antenna
%         for k = 1:Nt % for every five timesteps
%             for idx = 1:Nreg % for all modules + tot
%                 full_angle_avg(i,j,k,idx) = mean(full_angle(i,j,k,idx))/pi*180; % average over 5 steps and change to angle
%             end
%         end
%     end
% 
% end

full_angle_avg = full_angle/pi*180; % rad2deg

% find average/min/max over all antennas
for i = 1:Nf
    for j = 1:Nt
        for idx = 1:Nreg
            full_angle_mean(i,j,idx) = mean(full_angle_avg(:,i,j,idx)); % average of each antenna
            full_angle_min (i,j,idx) = min(full_angle_avg(:,i,j,idx));  % minimum of each antenna
            full_angle_max(i,j,idx)  = max(full_angle_avg(:,i,j,idx));  % maximum of each antenna
        end
    end
end

% for i = 1:Nf
%     if full_angle_mean(i,1,5) -180 > full_angle_min(i,1,5)
%         full_angle_min(i,:,5) = full_angle_min(i,:,5) + 180;
%     end
%     if full_angle_max(i,1,5) - 180 > full_angle_mean(i,1,5)
%         full_angle_max(i,:,5) = full_angle_max(i,:,5) -180;
%     end
% end

% allocate space for speed
max_phase_error = zeros(Nreg+1,Nf,Nt);

% find errors between phase/time and region
for i = 1:Nf % for each frequency
    for j = 1:Nt % for each 5mins
        for k = 1:Nreg % for each module + tot
            error1 = (full_angle_max(i,j,k) -full_angle_mean(i,j,k)); % difference in degree from max
            error2 = (full_angle_mean(i,j,k)-full_angle_min(i,j,k));  % difference in degree from min
            
            % find biggest angle difference and make it max error
            if max_phase_error(k,i,j)<error1
                max_phase_error(k,i,j) = error1;
            end
            if max_phase_error(k,i,j)<error2
                max_phase_error(k,i,j) = error2;
            end
        end
            % find max possible phase difference
            max_phase_error(6,i,j) = (sum(full_angle_max(i,j,1:4))-sum(full_angle_min(i,j,1:4)));
    end
end

%% figures

% figure(1) % figure showing errors/deviation in magnitude and rad 
% 
% for i = 1:2
%   
%     subplot(2,2,1)
%     errorbar(1:Nf, mean_lin_LNAx(:,i),mean_lin_LNAx(:,i)-min_lin_LNAx(:,i) , max_lin_LNAx(:,i)-mean_lin_LNAx(:,i))
%     hold on
%     errorbar(1:Nf, mean_lin_FEMx(:,i),mean_lin_FEMx(:,i)-min_lin_FEMx(:,i) , max_lin_FEMx(:,i)-mean_lin_FEMx(:,i))
%     errorbar(1:Nf, mean_lin_FOx(:,i) ,mean_lin_FOx(:,i)-min_lin_FOx(:,i)  , max_lin_FOx(:,i)-mean_lin_FOx(:,i))
%     errorbar(1:Nf, mean_lin_preADUx(:,i),mean_lin_preADUx(:,i)-min_lin_preADUx(:,i) , max_lin_preADUx(:,i)-mean_lin_preADUx(:,i))
%     hold off
%     xlabel('Timestamp nr [-]')
%     legend('LNA','FEM','FO','preADU')
%     title(['|Gain| deviation xpol at each timestamp' num2str(i) ' for each frequency' ])
%     
%     subplot(2,2,2)
%     errorbar(1:Nf, mean_lin_totx(:,i), mean_lin_totx(:,i)-min_lin_totx(:,i), max_lin_totx(:,i)-mean_lin_totx(:,i))
%     ylabel('Amp gain')
%     
%     subplot(2,2,3)
%     errorbar(1:Nf, mean_ph_LNAx(:,i),mean_ph_LNAx(:,i)-min_ph_LNAx(:,i) , max_ph_LNAx(:,i)-mean_ph_LNAx(:,i))
%     hold on
%     errorbar(1:Nf, mean_ph_FEMx(:,i),mean_ph_FEMx(:,i)-min_ph_FEMx(:,i) , max_ph_FEMx(:,i)-mean_ph_FEMx(:,i))
%     errorbar(1:Nf, mean_ph_FOx(:,i) ,mean_ph_FOx(:,i)-min_ph_FOx(:,i)  , max_ph_FOx(:,i)-mean_ph_FOx(:,i))    
%     errorbar(1:Nf, mean_ph_preADUx(:,i),mean_ph_preADUx(:,i)-min_ph_preADUx(:,i) , max_ph_preADUx(:,i)-mean_ph_preADUx(:,i))
%     hold off
%     xlabel('Timestamp nr [-]')
%     legend('LNA','FEM','FO','preADU')
%     title('phase deviation xplot')
%     
%     subplot(2,2,4)
%     errorbar(1:Nf, mean_ph_totx(:,i),mean_ph_totx(:,i)-min_ph_totx(:,i) , max_ph_totx(:,i)-mean_ph_totx(:,i))
%     ylabel('Amp gain')
%     pause(0.05)
%     
% end
%%
figure(2) % figure showing error/deviation in db and rad 

for i = 1:2 % for every timestep
    
    subplot(2,2,1)
    errorbar(1:Nf, 20*log10(mean_lin_LNAx(:,i)),20*log10(mean_lin_LNAx(:,i))-20*log10(min_lin_LNAx(:,i)) ...
                 , 20*log10(max_lin_LNAx(:,i))-20*log10(mean_lin_LNAx(:,i)))
    hold on
    errorbar(1:Nf, 20*log10(mean_lin_FEMx(:,i)),20*log10(mean_lin_FEMx(:,i))-20*log10(min_lin_FEMx(:,i)) ...
                 , 20*log10(max_lin_FEMx(:,i))-20*log10(mean_lin_FEMx(:,i)))
    errorbar(1:Nf, 20*log10(mean_lin_FOx(:,i)) ,20*log10(mean_lin_FOx(:,i))-20*log10(min_lin_FOx(:,i)) ...
                 , 20*log10(max_lin_FOx(:,i))-20*log10(mean_lin_FOx(:,i)))
    errorbar(1:Nf, 20*log10(mean_lin_preADUx(:,i)),20*log10(mean_lin_preADUx(:,i))-20*log10(min_lin_preADUx(:,i)) ...
                 , 20*log10(max_lin_preADUx(:,i))-20*log10(mean_lin_preADUx(:,i)))
    hold off
    xlabel('Frequency [MHz]')
    ylabel('Amp gain dB')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    legend('LNA','FEM','FO','preADU')
    title(['dB(|Gain|) deviation xpol at timestamp' num2str(i) ])
    
    subplot(2,2,2)
    errorbar(1:Nf, 20*log10(mean_lin_totx(:,i)), mag2db(mean_lin_totx(:,i))-20*log10(min_lin_totx(:,i))...
                 , 20*log10(max_lin_totx(:,i))-mag2db(mean_lin_totx(:,i)))
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    title('Total signal chain dB(|Gain|) devation xpol')
    
    subplot(2,2,3)
    errorbar(1:Nf, (mean_ph_LNAx(:,i)),(mean_ph_LNAx(:,i)-min_ph_LNAx(:,i)) , (max_ph_LNAx(:,i)-mean_ph_LNAx(:,i)))
    hold on
    errorbar(1:Nf, (mean_ph_FEMx(:,i)),(mean_ph_FEMx(:,i)-min_ph_FEMx(:,i)) , (max_ph_FEMx(:,i)-mean_ph_FEMx(:,i)))
    errorbar(1:Nf, (mean_ph_FOx(:,i)),(mean_ph_FOx(:,i)-min_ph_FOx(:,i)) , (max_ph_FOx(:,i)-mean_ph_FOx(:,i)))  
    errorbar(1:Nf, (mean_ph_preADUx(:,i)),(mean_ph_preADUx(:,i)-min_ph_preADUx(:,i)) , (max_ph_preADUx(:,i))-mean_ph_preADUx(:,i))
    hold off
    xlabel('Frequency  [MHz]')
    ylabel('\angle devation [rad]')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)

    legend('LNA','FEM','FO','preADU')
    title(['\angle deviation xpol at timestamp' num2str(i)])
    
    subplot(2,2,4)
    errorbar(1:Nf, mean_ph_totx(:,i),mean_ph_totx(:,i)-min_ph_totx(:,i) ,max_ph_totx(:,i)-mean_ph_totx(:,i))
    ylabel('\angle deviation [rad]')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    title('Total signal chain \angle devation xpol')
    pause(0.05)
end


%%
figure(3) 
subplot(1,5,1)
plot(1:Nf, 20*log10(Means_ant(:,:,1)))
axis([0 Nf, 40 50])
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain [dB]')

subplot(1,5,2)
plot(1:Nf, 20*log10(Means_ant(:,:,2)))
axis([0 Nf, 55 65])
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain [dB]')

subplot(1,5,3)
plot(1:Nf, 20*log10(Means_ant(:,:,3)))
axis([0 Nf, -2 2])
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain [dB]')

subplot(1,5,4)
plot(1:Nf, 20*log10(Means_ant(:,:,4)))
axis([0 Nf, -2 2])
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain [dB]')

subplot(1,5,5)
plot(1:Nf, 20*log10(Means_ant(:,:,5)))
axis([0 Nf, 95 110])
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain [dB]')

%%
for i = 1:Nf
figure(4) 
subplot(1,2,1)
plot(1:Nt/5, 100*squeeze(Max_error_stage(6,i,:)))
legend('Max possible error')
xlabel('Hour of the day ')
ylabel('Error in %')
title(['Maximum possible error frequency: ' num2str(f(i)/1e6) 'MHz'])
axis([0 Nt/5, 12 13.5])
set(gca,'XTick',0:24:Nt/5)
set(gca,'XTickLabel',0:2:24) 

subplot(1,2,2)
plot(1:Nt/5, 100*squeeze(Max_error_stage(1,i,:)))
hold on
plot(1:Nt/5, 100*squeeze(Max_error_stage(2,i,:)))
plot(1:Nt/5, 100*squeeze(Max_error_stage(5,i,:)))
hold off
axis([0 Nt/5, 5 8])
legend('LNA max error','FEM max error','tot max error')
title(['Max error all antennas frequency: ' num2str(f(i)/1e6) 'MHz'])
set(gca,'XTick',0:24:Nt/5)
set(gca,'XTickLabel',0:2:24) 
pause(0.05)
end


%%
figure(5)
for i = 1:Nf
    plot(1:Nt, squeeze(full_angle_avg(1,i,:,:)))
    title(['Average phase over 5mins for each module at freq: ' num2str(f(i)/1e6) ' MHz ' ])
    legend('LNA','FEM','FO','preADU','tot')
    pause(0.1)
end
%%
figure(6)
% Shows difference between antennas, marginal differences are observed
for i = 1:Nant
    plot(1:Nt, squeeze(full_angle_avg(i,1,:,:)))
    title(['Average phase over 5mins for each module at antenna: ' num2str(i)])
    legend('LNA','FEM','FO','preADU','tot')
    pause(0.1)
end

%%
figure(7)
for i = 1:Nf
    plot(1:Nt/5, squeeze(full_angle_max(i,:,3)))
    hold on
    plot(1:Nt/5, squeeze(full_angle_min(i,:,3)))
    hold off
    % legend('LNA','FEM','FO','preADU','tot')
    title(['Fibre Optic phase change for frequency: ' num2str(f(i)/1e6) 'MHz'] )
    pause(0.1)
end


%%
figure(8)
for i = 1:Nant
   
    subplot(1,5,1)
    imagesc(transpose(squeeze(Ph_mean(i,:,:,1))))
    colorbar
    title('LNA')
    xlabel('Frequency MHz')
    ylabel('Hour of measurement')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    set(gca,'YTick',0:24:Nt/5)
    set(gca,'YTickLabel',0:2:24) 
    
    subplot(1,5,2)
    imagesc(transpose(squeeze(Ph_mean(i,:,:,2))))
    colorbar
    title('FEM')
    xlabel('Frequency MHz')
    ylabel('Hour of measurement')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    set(gca,'YTick',0:24:Nt/5)
    set(gca,'YTickLabel',0:2:24)  
    
    subplot(1,5,3)
    imagesc(transpose(squeeze(Ph_mean(i,:,:,3))))
    colorbar
    title('FO')
    xlabel('Frequency MHz')
    ylabel('Hour of measurement')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    set(gca,'YTick',0:24:Nt/5)
    set(gca,'YTickLabel',0:2:24)  
    
    subplot(1,5,4)
    imagesc(transpose(squeeze(Ph_mean(i,:,:,4))))
    colorbar
    title('preADU')
    xlabel('Frequency MHz')
    ylabel('Hour of measurement')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    set(gca,'YTick',0:24:Nt/5)
    set(gca,'YTickLabel',0:2:24) 
    
    subplot(1,5,5)
    imagesc(transpose(squeeze(Ph_mean(i,:,:,5))))
    colorbar
    title('total')
    xlabel('Frequency MHz')
    ylabel('Hour of measurement')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    set(gca,'YTick',0:24:Nt/5)
    set(gca,'YTickLabel',0:2:24) 
    sgtitle(['Phase mean over 5 mins, receive path phases for antenna: ' num2str(i)])
pause(0.05)
end
%%
for i = 1:Nf
    figure(9)
    subplot(1,5,1)
    plot(1:Nt, squeeze(full_angle_max(i,:,1)))
    hold on
    plot(1:Nt, squeeze(full_angle_mean(i,:,1)))
    plot(1:Nt, squeeze(full_angle_min(i,:,1)))
    hold off
    title(['Phase error LNA for ' num2str(f(i)/1e6) ' MHz'])
    legend('max','mean','min')
    xlabel('Hour of the day ')    
    axis([0 Nt, -360 1080])
    set(gca,'XTick',0:120:Nt)
    set(gca,'XTickLabel',0:2:24)
    
    subplot(1,5,2)
    plot(1:Nt, squeeze(full_angle_max(i,:,2)))
    hold on
    plot(1:Nt, squeeze(full_angle_mean(i,:,2)))
    plot(1:Nt, squeeze(full_angle_min(i,:,2)))
    hold off
    title(['Phase error FEM for ' num2str(f(i)/1e6) ' MHz'])
    legend('max','mean','min')
    xlabel('Hour of the day ')
    axis([0 Nt, -360 1080])
    set(gca,'XTick',0:120:Nt)
    set(gca,'XTickLabel',0:2:24)
    
    subplot(1,5,3)
    plot(1:Nt, squeeze(full_angle_max(i,:,3)))
    hold on
    plot(1:Nt, squeeze(full_angle_mean(i,:,3)))
    plot(1:Nt, squeeze(full_angle_min(i,:,3)))
    hold off
    title(['Phase error FO for ' num2str(f(i)/1e6) ' MHz'])
    legend('max','mean','min')
    xlabel('Hour of the day ')  
    axis([0 Nt, -360 1080])
    set(gca,'XTick',0:120:Nt)
    set(gca,'XTickLabel',0:2:24)
    
    subplot(1,5,4)
    plot(1:Nt, squeeze(full_angle_max(i,:,4)))
    hold on
    plot(1:Nt, squeeze(full_angle_mean(i,:,4)))
    plot(1:Nt, squeeze(full_angle_min(i,:,4)))
    hold off
    title(['Phase error preADU for ' num2str(f(i)/1e6) ' MHz'])
    legend('max','mean','min')
    xlabel('Hour of the day ')   
    axis([0 Nt, -360 1080])
    set(gca,'XTick',0:120:Nt)
    set(gca,'XTickLabel',0:2:24)
    
    subplot(1,5,5)
    plot(1:Nt, squeeze(full_angle_max(i,:,5)))
    hold on
    plot(1:Nt, squeeze(full_angle_mean(i,:,5)))
    plot(1:Nt, squeeze(full_angle_min(i,:,5)))
    hold off
    title(['Phase error total for ' num2str(f(i)/1e6) ' MHz'])
    legend('max','mean','min')
    xlabel('Hour of the day ') 
    axis([0 Nt, -360 1080])
    set(gca,'XTick',0:120:Nt)
    set(gca,'XTickLabel',0:2:24)
    
    pause(0.5)
end
%%
for i = 1:Nf
figure(10) 
subplot(1,2,1)
plot(1:Nt, 100*squeeze(max_phase_error(5,i,:)))
legend('Max possible error')
xlabel('Hour of the day ')

axis([0 Nt, -inf inf])
set(gca,'XTick',0:24:Nt/5)
set(gca,'XTickLabel',0:2:24) 
ylabel('Error in %')
title(['Maximum possible error frequency: ' num2str(f(i)/1e6) 'MHz'])
subplot(1,2,2)
plot(1:Nt, squeeze(max_phase_error(1,i,:)))
hold on
plot(1:Nt, squeeze(max_phase_error(2,i,:)))
plot(1:Nt, squeeze(max_phase_error(3,i,:)))
plot(1:Nt, squeeze(max_phase_error(4,i,:)))
plot(1:Nt, squeeze(max_phase_error(5,i,:)))
hold off
axis([0 Nt, 0 100])
legend('LNA max error','FEM max error','FO max error','preADU max error','tot max error')
title(['Max error all antennas frequency: ' num2str(f(i)/1e6) 'MHz'])
set(gca,'XTick',0:24:Nt/5)
set(gca,'XTickLabel',0:2:24) 
pause(0.05)
end

%%
figure(11)

for i = 1:Nf % for every frequency
    
    subplot(2,2,1)
    errorbar(1:10:Nt, 20*log10(mean_lin_LNAx(i,1:10:Nt)),20*log10(mean_lin_LNAx(i,1:10:Nt))-20*log10(min_lin_LNAx(i,1:10:Nt)) ...
                 , 20*log10(max_lin_LNAx(i,1:10:Nt))-20*log10(mean_lin_LNAx(i,1:10:Nt)))
    hold on
    errorbar(1:10:Nt, 20*log10(mean_lin_FEMx(i,1:10:Nt)),20*log10(mean_lin_FEMx(i,1:10:Nt))-20*log10(min_lin_FEMx(i,1:10:Nt)) ...
                 , 20*log10(max_lin_FEMx(i,1:10:Nt))-20*log10(mean_lin_FEMx(i,1:10:Nt)))
    errorbar(1:10:Nt, 20*log10(mean_lin_FOx(i,1:10:Nt)) ,20*log10(mean_lin_FOx(i,1:10:Nt))-20*log10(min_lin_FOx(i,1:10:Nt)) ...
                 , 20*log10(max_lin_FOx(i,1:10:Nt))-20*log10(mean_lin_FOx(i,1:10:Nt)))
    errorbar(1:10:Nt, 20*log10(mean_lin_preADUx(i,1:10:Nt)),20*log10(mean_lin_preADUx(i,1:10:Nt))-20*log10(min_lin_preADUx(i,1:10:Nt)) ...
                 , 20*log10(max_lin_preADUx(i,1:10:Nt))-20*log10(mean_lin_preADUx(i,1:10:Nt)))
    hold off
    axis([0 1440, -inf inf])
    xlabel('Hour of measurement')
    ylabel('Amp gain [dB]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    legend('LNA','FEM','FO','preADU')
    title(['dB(|Gain|) deviation xpol at timestamp' num2str(i) ])
    
    subplot(2,2,2)
    errorbar(1:10:Nt, 20*log10(mean_lin_totx(i,1:10:Nt)), mag2db(mean_lin_totx(i,1:10:Nt))-20*log10(min_lin_totx(i,1:10:Nt))...
                 , 20*log10(max_lin_totx(i,1:10:Nt))-mag2db(mean_lin_totx(i,1:10:Nt)))
    axis([0 1440, -inf inf])
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    title('Total signal chain dB(|Gain|) devation xpol')
    
    subplot(2,2,3)
    errorbar(1:10:Nt, (mean_ph_LNAx(i,1:10:Nt)),(mean_ph_LNAx(i,1:10:Nt)-min_ph_LNAx(i,1:10:Nt)) , (max_ph_LNAx(i,1:10:Nt)-mean_ph_LNAx(i,1:10:Nt)))
    hold on
    errorbar(1:10:Nt, (mean_ph_FEMx(i,1:10:Nt)),(mean_ph_FEMx(i,1:10:Nt)-min_ph_FEMx(i,1:10:Nt)) , (max_ph_FEMx(i,1:10:Nt)-mean_ph_FEMx(i,1:10:Nt)))
    errorbar(1:10:Nt, (mean_ph_FOx(i,1:10:Nt)),(mean_ph_FOx(i,1:10:Nt)-min_ph_FOx(i,1:10:Nt)) , (max_ph_FOx(i,1:10:Nt)-mean_ph_FOx(i,1:10:Nt)))  
    errorbar(1:10:Nt, (mean_ph_preADUx(i,1:10:Nt)),(mean_ph_preADUx(i,1:10:Nt)-min_ph_preADUx(i,1:10:Nt)) ,...
                   (max_ph_preADUx(i,1:10:Nt))-mean_ph_preADUx(i,1:10:Nt))
    hold off
    axis([0 1440, -inf inf])
    xlabel('Hour of measurement')
    ylabel('\angle devation [rad]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    legend('LNA','FEM','FO','preADU')
    title(['\angle deviation xpol at timestamp' num2str(i)])
    
    subplot(2,2,4)
    errorbar(1:10:Nt, mean_ph_totx(i,1:10:Nt),mean_ph_totx(i,1:10:Nt)-min_ph_totx(i,1:10:Nt) ,max_ph_totx(i,1:10:Nt)-mean_ph_totx(i,1:10:Nt))
    axis([0 1440, -inf inf])
    ylabel('\angle deviation [rad]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    title('Total signal chain \angle devation xpol')
    pause(0.05)
end

%%
for i = 1:Nf
figure(12)
histogram(mag2db(squeeze(abs(g_totx(:,i,1)))))
histfit(mag2db(squeeze(abs(g_totx(:,i,1)))))

xlabel('Total gain magnitude [dB]')
ylabel('Nr antennas between gain interval [-]')
axis([97.5 110, 0 45])
pause(0.5)
end
%% change of antenna 1
mean_lin = zeros(Nt/5, Nf);
phase_lin = zeros(Nt/5,Nf);
for ant = 1:256
    for i = 1:Nt/5
        for j = 1:Nf
            mean_lin(i,j) = mean(abs(gLNAx(ant,j,(i-1)*5+1:5*i)));
            phase_lin(i,j) = mean(angle(gLNAx(ant,j,(i-1)*5 + 1:5*i)));
        end
    end
end

figure(88)
subplot(1,2,1)
imagesc(mean_lin)
colorbar
xlabel('Frequency [MHz]')
ylabel('Every 5th timestep')
title('Gain LNA')
set(gca,'XTick',0:5:61)
set(gca,'XTickLabel',50:25:350)
set(gca,'YTick',0:24:Nt/5)
set(gca,'YTickLabel',0:2:24) 

subplot(1,2,2)
imagesc(phase_lin)
colorbar
xlabel('Frequency MHz')
ylabel('Every 5th timestep')
title('Phase LNA [rad]')
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
set(gca,'YTick',0:24:Nt/5)
set(gca,'YTickLabel',0:2:24) 
%%


