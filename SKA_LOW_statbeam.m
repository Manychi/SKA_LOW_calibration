function J_beam = SKA_LOW_statbeam(l0, m0, f, gain, l_grid, m_grid, x_pos, y_pos)
% Jbeam = SKA_LOW_statbeam(l0, m0, f, gain, l_grid, m_grid)
%
% This function predicts the SKA-LOW station response in terms of Jones
% matrices towards the specified directions while the station beam is
% pointed towards (l0, m0). The receive path gains are taken into account.
%
% Arguments
% l0    : l-coordinate of pointing direction
% m0    : m-coordinate of pointing direction
% freq  : operating frequency of the station in Hz
% g     : (2 * Nant)-element vector with receive path gains assumed to be
%         ordered as [g_{1,x}, g_{1,y}, ..., g_{Nant,x}, g_{Nant,y}]
% l     : Ndir-element vector with l-coordinates of directions of interest
% m     : Ndir-element vector with m-coordinates of directions of interest
%
% Return value
% Jbeam : 2-by-2-by-Ndir tensor describing the station response
%
% Stefan J. Wijnholds, 10 August 2020 
% Rewritten at 5-8-2020 to be similar to the rest of the code from Ivar
%% Variables
% x_pos = Arraylayout(:, 1); % Get xpositions from antenna 1->256
% y_pos = Arraylayout(:, 2); % Get ypositions from antenna 1->256

c = 2.99792e8;             % speed of light (m/s)

%% Perform phase vector calc and calc Jones_mat for specified directions in lm
% calculate beamformer weights
weight = exp((-2*pi*1j*f/c)*(x_pos*l0 + y_pos*m0));  % calc individual weights
weight = kron(weight, [1; 1]);                       % augment to two polarisations

% determine element responses in specified directions
J_elem = pol_EEP_SKALA4AL(l_grid, m_grid ,x_pos , y_pos, f); % perform pol_EEP_SKALA4AL(l,m,x_pos, y_pos)

%% determine station response by Jones_beam
Ndata     = length(l_grid);                     % get length of lm i.e. datapoints 
Nant      = length(x_pos);                      % get number of antennas
J_weighted = zeros(2, 2, Ndata, Nant);          % Allocate memory for speed

for i = 1:Ndata % for each datapoint
    for j = 1:Nant % for each antenna
        J_weighted(:, :, i, j) = diag(weight(2*j-1:2*j)) * diag(gain(2*j-1:2*j)) * squeeze(J_elem(:, :, j, i)); %calc Jones
    end
end
J_beam = sum(J_weighted, 4);