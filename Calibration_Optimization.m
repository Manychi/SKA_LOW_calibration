%% Calibration_Optimization.
%
% The idea of this script is to find optimizations within the calibration
% procedures which can then result in less computation needed. One part of
% this is the interpolation of results from multiple points for the receive
% path gain and use that as a basis for calibration reducing the amount of
% times calibration is required. A comparison will be made showing the
% detrement of this procedure.
% 
% % % clear up workspace, just to be sure
% clear all
% clc
close all
%% Variables

Fnum  = 13;   % Frequency index corresponding to 110MHz
err   = 1e-5; % max difference between iterations
imax  = 300 ;  % max number of iterations
Time  = 1;    % minute of measurement 1:1440
MVDR  = 0;    % set 1 if MVDR as a weight is used
Ran   = 1;    % set to 0 if need to load values and generate covariance matrices
Ndata = 201;  % amount of datapoints in lm coordinates

%% Run required scripts for variables.
if Ran == 0
run('./Receive_path_gain/receive_path_gain.m') % run receive path gain for g_totx
run('./Cov_Matrix_gen.m')                      % run Cov matrix get to get covarience matrix to see results after calibration
run('./Weights_Beamforming.m')                 % get weights for holography
disp('done with prep')
end
%% Find fit model using fit function
% calc new values 
step_eq  = Nt/Nft;       % step s.t. Nt is also 61 long
t_new    = 1:step_eq:Nt; % get every new time indication
t_new    = round(t_new); % make sure t_new is a proper number

% get gains
Gain_means = squeeze(mean(abs(g_totx),1)); % get gain mean over all antennas
Gain_new   = Gain_means(:,t_new);          % get gain from means per new timestep

Phase_means= squeeze(mean(unwrap(angle(g_totx),1))); 

% [X,Y]     = meshgrid(1:size(Gain_new,2), 1:size(Gain_new,1)); % get grid as function of time and frequency
% fit_model = fit([X(:), Y(:)], abs(Gain_new(:)), 'poly42');    % good fit is poly42
% 
% % plot figure showing fitted model using fit and the gain itself
% figure(55)
% plot(fit_model, [X(:), Y(:)], abs(Gain_new(:)))

%% find points in frequency for magnitude gain
fit_f   = polyfit(f,mag2db(Gain_means(:,1)),2); % 2nd order plynomial functions
g_fit_f = polyval(fit_f,f);                     % predicted values based on polyfit

deri_g_f   = diff(diff(mag2db(abs(Gain_means(:,1))))); % calc 2nd derivation for points of interest

% find frequencies of importance and get gains
frqs        = [freq_tot(1),freq_tot(abs(deri_g_f(:)) > 0.021),freq_tot(Nft)]; % if curvature is too high take frequency
[~, index_f]= ismember(frqs, freq_tot);                                        % take all indices
Pts_f       = mag2db(abs(Gain_means(index_f,1)));

% get first order fit for a line, and make new gain
for i = 1:length(Pts_f)-1 % for all points -1
    Fit_f                               = polyfit(frqs([i,i+1]), Pts_f([i,i+1]),1);                % get slope and offset
    Fit_f_g(index_f(i):1:index_f(i+1)) = Fit_f(1)*freq_tot(index_f(i):1:index_f(i+1)) + Fit_f(2); % fill in estimated gain
end

% figure to check
figure
plot(1:Nft, Fit_f_g)
hold on
plot(1:Nft, mag2db(abs(Gain_means(:,1))))
plot(1:Nft, g_fit_f)
grid on
axis([1 61, -inf inf])
legend('Fitted gain', 'measured gain', 'Fit full')
set(gca,'XTick',1:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Amplitude gain [dB]')
%title(['Gain comparison for nr of points in f:' num2str(length(frqs))])

%% find points in time for magnitude gain
fit_t   = polyfit(t,mag2db(Gain_means(Fnum,:)),4); % fourth order fit function
g_fit_t = polyval(fit_t,t);                          % predicted values based on polyfit

% find points based on peaks, and extra points added for phase.
[~, locs] = findpeaks(Gain_means(Fnum,:));           % find indexes
t_pts     = [t(1); t(locs); t(765); t(900); t(Nt) ]; % points of interest in time
t_pts     = sort(t_pts);                             % sort s.t. polyfit is accurate
Ntpts     = length(t_pts);                           % get length of points in time

[~, indexs]= ismember((t_pts), t);                   % get indexes of all time points ( can be done with locs and add extra points
Pts_t      = (abs(Gain_means(Fnum,(indexs))));       % get all magnitude gains of interest

% fit for all time measurement points and make first order function between
% all values
for i = 1:length(Pts_t) -1
   Fit_t = polyfit(t_pts([i,i+1]), Pts_t([i,i+1]),1);
   Fit_t_g(indexs(i):1:indexs(i+1)) = Fit_t(1)*t(indexs(i):1:indexs(i+1)) + Fit_t(2);   
end

figure % figure comparing polyfit for all, the actual mean, more elaborate robust fit
t1 = t/3600 - 18;
plot(t1, mag2db(Fit_t_g))
hold on
plot(t1, mag2db(Gain_means(Fnum,:)))
plot(t1, g_fit_t)
grid on
axis([0 24, -inf inf])
xlabel('Hour of measurement')
ylabel('Amplitude gain [dB]') 
legend( 'Fitted gain', 'Measured gain','Fit full')



%% Phase calibration estimation in time at 110MHz
Phase_mean_time = unwrap(squeeze(Phase_means(Fnum,:))); % unwarp for proper estimation
Phase_t         = Phase_mean_time(indexs);              % find phase moments of interest based on gain points of interest

% fit for all time measurement points and make 1st ord function for Nt
for i = 1:length(Pts_t)-1
    Fit_p   = polyfit(t_pts([i,i+1]), Phase_t([i,i+1]),1);
    Fit_t_p(indexs(i):1:indexs(i+1)) = Fit_p(1)*t(indexs(i):1:indexs(i+1)) + Fit_p(2);
end

figure % figure comparing for unwrap and the actual value
plot(t1, rad2deg(Phase_mean_time(:)))
hold on
plot(t1, rad2deg(Fit_t_p))
grid on
axis([0 24, -inf inf])
xlabel('Hour of measurement')
ylabel('Gain phase [\circ]') 
legend( 'Fitted gain', 'Measured gain')

%% Calibrate by use StEFCal at 950, interpolate from 691 and 1023, and different interpolation implementations
R_model = R; % get R from Cov Matrix Gen, assumed it remains constant in time.

moments     = [691, 950, 1023]; % points chosen in time where the longest break is

% allocate memory for speed
G_chain_t1     = zeros(2*Nant,1);
G_chain_t2     = zeros(2*Nant,1);
G_chain_t3     = zeros(2*Nant,1);

% for all points in [moments] get gain in x and y polarization
for i = 1:Nant % for each antenna
    G_chain_t1(i*2-1) = squeeze(g_totx(i,Fnum,moments(1))); % signal chain gain x polarization are all odd number
    G_chain_t1(i*2)   = squeeze(g_toty(i,Fnum,moments(1))); % signal chain gain y polarization even numbers
end
        
for i = 1:Nant % for each antenna
    G_chain_t2(i*2-1) = squeeze(g_totx(i,Fnum,moments(2))); % signal chain gain x polarization are all odd number
    G_chain_t2(i*2)   = squeeze(g_toty(i,Fnum,moments(2))); % signal chain gain y polarization even numbers
end
        
for i = 1:Nant % for each antenna
    G_chain_t3(i*2-1) = squeeze(g_totx(i,Fnum,moments(3))); % signal chain gain x polarization are all odd number
    G_chain_t3(i*2)   = squeeze(g_toty(i,Fnum,moments(3))); % signal chain gain y polarization even numbers
end

% get covariance model for all moments
R_meas_opt1  = diag(G_chain_t1)*R_model*diag(G_chain_t1)';  % Calc measured covariance matrix
R_meas_opt2  = diag(G_chain_t2)*R_model*diag(G_chain_t2)';  % Calc measured covariance matrix
R_meas_opt3  = diag(G_chain_t3)*R_model*diag(G_chain_t3)';  % Calc measured covariance matrix
    
% normalize each covariance matrix
norm_fact(1) = abs(R_meas_opt1(1,1));
norm_fact(2) = abs(R_meas_opt2(1,1));
norm_fact(3) = abs(R_meas_opt3(1,1));

R_meas_opt1 = R_meas_opt1/(abs(R_meas_opt1(1,1))); 
R_meas_opt2 = R_meas_opt2/(abs(R_meas_opt2(1,1))); 
R_meas_opt3 = R_meas_opt3/(abs(R_meas_opt3(1,1))); 

% Calibrate || could try to get in a for loop, didn't provide accurate
% results when i tried. Due to time issues this lenghty implementation.
[G_stef_t1, i_stef_t1]    = gainsolv(err,R_model, R_meas_opt1, ones(2*Nant,1),imax);
g_stef_new_t1             = squeeze(G_stef_t1(:,i_stef_t1-1));

[G_stef_t2, i_stef_t2]    = gainsolv(err,R_model, R_meas_opt2, ones(2*Nant,1),imax);
g_stef_new_t2             = squeeze(G_stef_t2(:,i_stef_t2-1));

[G_stef_t3, i_stef_t3]    = gainsolv(err,R_model, R_meas_opt3, ones(2*Nant,1),imax);
g_stef_new_t3             = squeeze(G_stef_t3(:,i_stef_t3-1));

% allocate memory for speed
g_new = zeros(2*Nant,1);

% for each antenna do polyfit based on results from moment 1 and 3 ||
% gvalues and pvalues can be combined i think.
for i = 1:2*Nant % for each antenna and its two polarizations
    gvalues  = [abs(g_stef_new_t1(i)), abs(g_stef_new_t3(i))];
    pvalues  = [angle(g_stef_new_t1(i)), angle(g_stef_new_t3(i))];
    G_comp   = polyfit(t([moments(1), moments(3)]), gvalues, 1);
    P_comp   = polyfit(t([moments(1), moments(3)]), pvalues, 1);
    g_new(i) = (G_comp(1)*t(moments(2))+ G_comp(2))*exp(-1i*(P_comp(1)*t(moments(2)) + P_comp(2))); 
end
%% interpolation options
interp_gain               = Fit_t_g(moments(2))*exp(-1i*Fit_t_p(moments(2))); %get result based on earlier analysis

% get offsets based on the histogram assuming continuity for gain and phase
gain_offsets  = abs(g_totx(:,Fnum,moments(2)))- mean(abs(g_totx(:,Fnum,moments(2))));
phase_offsets = angle(g_totx(:,Fnum,moments(2))) - mean(angle(g_totx(:,Fnum,moments(2))));

% based on the interpreted gain of the results and offsets get complex gain
% interpolated value
Gain_test = (gain_offsets+abs(interp_gain)).*exp(-1i*(phase_offsets+angle(interp_gain)));

% allocate memory for speed
interped = zeros(2*Nant, moments(3)-moments(1) +1);

% use interp1 for the whole range of points between moment 1 and 3 
for i = 1:2*Nant % for each antenna and polarization
    interped(i,:) = interp1(t([moments(1) moments(3)]), [g_stef_new_t1(i) g_stef_new_t3(i)], t(moments(1):moments(3))); 
end
g_test = interped(:, moments(2)-moments(1));

%%
% quick setup to see if this gets reduced number of iterations while
% keeping accuracy
[g_stef_quick, i_stef_q]  = gainsolv(err, R_model, R_meas_opt2, interp_gain*ones(2*Nant,1), imax); 
g_quick_new               = squeeze(g_stef_quick(:,i_stef_q-1));
%% figures comparing the phase and magnitude gain 
figure
plot(1:Nant, -angle(g_stef_new_t2(1:2:end)), 'bo',...
     1:Nant, angle(1./G_chain_t2(1:2:end)), 'mx', ...
     1:Nant, angle(g_new(1:2:end)), 'r*')%,...
     %1:Nant, angle(Gain_test), 'r*')
legend('Gain Stef', 'Gain Ideal', 'Gain interp', 'Gain predict')
grid on
axis([0 256, -inf inf])
xlabel('Antenna nr [-]')
ylabel('Phase [rad]')

figure
plot(1:Nant, abs(1./g_stef_new_t2(1:2:end)/sqrt(norm_fact(2))), 'bo',...
     1:Nant, abs(1./G_chain_t2(1:2:end)), 'mx',...
     1:Nant, abs(1./g_new(1:2:end)/sqrt(norm_fact(2))), 'r*')%,...
     %1:Nant, abs(1./Gain_test), 'r*')
legend('Gain Stef', 'Gain Ideal', 'Gain interp', 'Gain predict')
grid on
axis([0 256, -inf inf])
xlabel('Antenna nr [-]')
ylabel('Magnitude gain')

%% Combine time and frequency next, and implement different R depending on time as well for what is observed.

