# SKA_LOW_calibration
Part of the thesis of Ivar Jansen - i.s.jansen@student.tue.nl, i-s.jansen@hotmail.com.
Files excluded in this repository are EEP_SKALA4AL_110MHz_38m.mat and test.h5 which are too large. 
The first should be added to the Receive_path_gain folder, and mat_files. The latter should only be added to Receive_path_gain. 

Overhead scripts are: Calibration_comparison.m - Equal_EEP  - Calibration_Optimization - Error_calc_full

For the first research question i.e. what are the errors/ deviations in gain we have Error_calc_full, 
which is the full version of Error_calc which calculates all variables, and Error_calc figures which plots all figures based on variables in Error_calc.m, which uses: Receive_path_gain/Receive_path_gain.m.

The second research question i.e. what are the computational differences and resolution differences when calibrating using:
Holography and self-calibration. 
For this we have Calibration_comparison.m which uses:
- Receive_path_gain/receive_path_gain.m
- Cov_Matrix_gen.m, which uses JulianDay, pol_EEP_SKALA4AL.m, JulianDay.m, raddeg2uv.m, radectolm.m, Khatri_Rao.m
- Weights_Beamforming.m,
- gainsolv.m,
- Self_calibration.m,
- Holography.m, and
- acm2skyimage.m.

The third research question about the impact of using a single EEP for all antennas uses:
- Equal_EEP.m
which uses:
- Receive_path_gain/receive_path_gain.m
- Cov_Matrix_gen.m, which uses JulianDay, pol_EEP_SKALA4AL.m, JulianDay.m, raddeg2uv.m, radectolm.m, Khatri_Rao.m
- Weights_Beamforming.m,
- gainsolv.m,
- Self-calibration.m,
- Holography.m, 
- acm2skyimage.m.

The fourth research question about interpolating results uses:
- Calibration_Optimization.m
which uses:
- Receive_path_gain/receive_path_gain.m
- Cov_Matrix_gen.m, which uses JulianDay, pol_EEP_SKALA4AL.m, JulianDay.m, raddeg2uv.m, radectolm.m, Khatri_Rao.m
- Weights_Beamforming.m,
- gainsolv.m,
- Self-calibration.m,
- Holography.m, 
- acm2skyimage.m.

