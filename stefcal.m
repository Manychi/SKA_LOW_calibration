function[Gain_result,i] = stefcal(R, M, error,imax)
%% Initialization of variables
Nant         = size(R,1);
G_initial    = ones(Nant,1); 
Norm_Columns = zeros(1,Nant);
R_norm       = R;
R_Column_Cal = complex(zeros(Nant), zeros(Nant));
Cal_Est      = complex(zeros(Nant,1), zeros(Nant,1));
Gain_result  = zeros(Nant,imax);
%% Cycle for calibration

for i = 1:Nant
    Norm_Columns(i) = R(:,i)'*R(:,i);
    R_norm(:,i)     = R(:,i)/Norm_Columns(i);
end

for i = 1:Nant
    R_Column_Cal(:,i) = G_initial.*M(:,i);
end

for i = 1:imax
    for j = 1:Nant
        Cal_Est(j) = R_norm(:,j)'*R_Column_Cal(:,j);
    end
    Gain = 1./conj(Cal_Est);
    if mod(i,2)>0
        First_Gain  = Gain;
    else
        Gain_old = Gain;
        Gain = (Gain+First_Gain)/2;
        if i>= 2
            Dir_Gain_Norm = norm(Gain-Gain_old);
            Gain_Norm = norm(Gain);
            if(Dir_Gain_Norm/Gain_Norm)<=error
                disp(['Stef_cal convergence reached after ' num2str(i) ' iterations']);
                break
            end
        end
    end
    if i == imax
        disp('No convergence reached');
    end
    for j = 1:Nant
        R_Column_Cal(:,j) = Gain.*R(:,j);
    end
    Gain_result(:,i) = Gain;
end
    