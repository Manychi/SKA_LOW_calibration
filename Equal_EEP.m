%% Equal EEP
% The aim of this script is to see what the effect is of using a single EEP
% for all individual antennas. This is taken care of in the subfunction
% pol_EEP_equal. Not only in terms of resolution, but also for
% computational load roughly indicated by the amount of iterations
% required. Note that holography in and of itself requires fewer
% computational power due to the ommision of the covarience matrix
%
% Self shows my own implementation of StEFCal
%
% Code made by Ivar Jansen as a part of Master Thesis 
% Contact using i-s.jansen@hotmail.com or i.s.jansen@student.tue.nl
% Updated for clarity and readability 5-9-2022
%
% % clear all to be sure, issues with receive_path_gain 
% % uncomment if ran = 0
% clear all
% clc
%% Variables

Fnum  = 13;   % Frequency index corresponding to 110MHz
err   = 1e-4; % max difference between iterations
imax  = 400 ;  % max number of iterations
Time  = 1;    % minute of measurement 1:1440
MVDR  = 0;    % set 1 if MVDR as a weight is used
Ran   = 1;    % set to 0 if need to load values and generate covariance matrices
Ndata = 201;  % amount of datapoints in lm coordinates

%% Run scripts for data 

if Ran == 0
run('./Receive_path_gain/receive_path_gain.m') % gets gains for receive path
run('./Cov_Matrix_gen.m')                      % calcs the cov matrix for each frequency of interest using individual eeps
run('./Weights_Beamforming.m')                 % calcs the weights used for holography
end
%% Calc Jones matrix for frequency given equal EEPS

%gain        = ones(2*Nant); % fill in initial gains as ones
J_EEP_freqs = pol_EEP_equal(l_grid, m_grid, x_pos, y_pos, f(Fnum));
  
%% Calculate Covarience equal EEP matrix

%Allocate memory for speed
Phase_Gain_vec  = zeros(2*Nant,Npol);
R_EEP           = zeros( 2 * Nant, 2 * Nant);

for i = 1:Nsrc % for each source
    for j = 1:Nant % for each antenna
        
        Phase_Gain_vec(2*j-1:2*j,:) = squeeze(J_EEP_freqs(:,:,j,Src_num(i))); % get phasevector * gain vector per antenna
    end
    
    R_EEP(:,:) = squeeze(R_EEP(:,:)) + (Phase_Gain_vec*sigma_src(i)*Phase_Gain_vec'); % get covarience
end

%% Calibration setup

R_model = squeeze(R(:,:));                % squeeze(R(Fnum,:,:));    % Get model covariance matrix
%R_model = R_model/(abs(R_model(1,1)));     % Normalize R_model 

G_chain = zeros(2*Nant,1);
for i = 1:Nant
% Get signal chain gain for f and t 
G_chain(i*2-1) = squeeze(g_totx(i,Fnum,Time));
G_chain(i*2)   = squeeze(g_toty(i,Fnum,Time));
end

R_meas  = diag(G_chain)*R_model*diag(G_chain)';  % Calc measured covariance matrix
norm_R  = abs(R_meas(1,1));
R_meas  = R_meas/(abs(R_meas(1,1)));             % Normalize R_measured

% Sigma_m = diag(G_chain(1:2:end))*squeeze(Sigma(:,:))*diag(G_chain(1:2:end))'; % Calc Signal covariance matrix
% Sigma_m = Sigma_m/max(abs(Sigma_m(:)));     % Normalzie Sigma_measured

% repeat for single EEP
R_m_EEP = (R_EEP(:,:));
%R_m_EEP = R_m_EEP/max(abs(R_m_EEP(:)));

R_E_meas= diag(G_chain)*R_m_EEP*diag(G_chain)';
R_E_meas= R_E_meas/(abs(R_E_meas(1,1)));

% Sig_EEP = diag(G_chain(1:2:end))*squeeze(Sigma(:,:))*diag(G_chain(1:2:end))';
% Sig_EEP = Sig_EEP/(abs(Sig_EEP(1,1)));

skymap        = acm2skyimage(R(1:2:end, 1:2:end), x_pos, y_pos, f(Fnum), l_size, l_size);
[~, max_holo] = max(skymap(:));
[idx1, idx2]  = ind2sub([Ndata,Ndata],max_holo);
Weight        = squeeze(Weights_BF(1,:, idx1, idx2)).';

%% Calibration

[G_self, i_self]           = Self_calibration(R_model, R_meas, err, imax);
[G_stef, i_stef]           = gainsolv(err,R_model, R_meas, ones(2*Nant,1),imax);
[G_holo, i_holo]           = Holography(R_meas(1:2:end,1:2:end), Weight, err, imax );

[G_EEP_self, i_self_EEP]       = Self_calibration(R_model, R_E_meas, err, imax);
[G_EEP_stef, i_stef_EEP]       = gainsolv(err,R_model, R_E_meas, ones(2*Nant,1),imax);
[G_EEP_holo, i_holo_EEP]       = Holography(R_E_meas(1:2:end, 1:2:end), Weight, err, imax );


% Add gains to show calibrated system
g_self_new                = squeeze(G_self(:,i_self));
g_stef_new                = squeeze(G_stef(:,i_stef-1));
g_holo_new                = squeeze(G_holo(:,i_holo));

g_self_eep                = squeeze(G_EEP_self(:,i_self));
g_stef_eep                = squeeze(G_EEP_stef(:,i_stef-1));
g_holo_eep                = squeeze(G_EEP_holo(:,i_holo));

% find new weight and make sure its pointing properly
R_holo         = diag(1./g_holo_new)*R_meas(1:2:end, 1:2:end)*diag(1./g_holo_new)';
skymap_holo    = acm2skyimage(R_holo, x_pos, y_pos, f(Fnum), l_size, l_size);
[~, maxidx]    = max(skymap_holo(:));
[idx1a, idx2a] = ind2sub([Ndata, Ndata], maxidx);

w1             = squeeze(Weights_BF(1, :, idx1a, idx2a)).'; % weights to point in the direction of maximum (geometrical delays)
g_holo_new     = g_holo_new .* (conj(w1) .* Weight); % apply pointing correction

% make phase of first element equal to phase of first element obtained by
% Self_Calibration to facilitate comparison
g_holo_new     = g_holo_new * exp(-1i * (angle(g_holo_new(1)) - angle(g_self_new(1))));

% repeat for equal EEP
R_holo_EEP     = diag(1./g_holo_eep)*R_meas(1:2:end, 1:2:end)*diag(1./g_holo_eep)';
skymap_holo_EEP= acm2skyimage(R_holo_EEP, x_pos, y_pos, f(Fnum), l_size, l_size);
[~, maxidx_e]  = max(skymap_holo(:));
[idx1e, idx2e] = ind2sub([Ndata, Ndata], maxidx_e);

w2             = squeeze(Weights_BF(1,:,idx1e, idx2e)).';
g_holo_eep     = g_holo_new*exp(-1i*(angle(g_holo_new(1))-angle(g_self_new(1))));

g_stef_eep     = g_stef_eep*exp(-1i*(angle(g_stef_eep(1))-angle(g_self_new(1))));
% update covariance matrix for each calibration method
R_self = diag(g_self_new)*R_meas*diag(g_self_new)';
R_stef = diag(g_stef_new)*R_meas*diag(g_stef_new)';
R_holo = diag(1./g_holo_new)*R_meas(1:2:end,1:2:end)*diag(1./g_holo_new)';

R_EEP_self = diag(g_self_eep)*R_E_meas*diag(g_self_eep)';
R_EEP_stef = diag(g_stef_eep)*R_E_meas*diag(g_stef_eep)';
R_EEP_holo = diag(1./g_holo_eep)*R_E_meas(1:2:end, 1:2:end)*diag(1./g_holo_eep)';


%% Visualization by use of acm2skyimage

Uncalibrated_map = acm2skyimage(squeeze(R(1:2:end,1:2:end))   , x_pos, y_pos, f(Fnum), l_size, l_size);
Map_self         = acm2skyimage(R_self(1:2:end,1:2:end)       , x_pos, y_pos, f(Fnum), l_size, l_size);
Map_holo         = acm2skyimage(R_holo                        , x_pos, y_pos, f(Fnum), l_size, l_size);
Map_stef         = acm2skyimage(R_stef(1:2:end,1:2:end)       , x_pos, y_pos, f(Fnum), l_size, l_size);

Uncal_EEP_map    = acm2skyimage(R_EEP(1:2:end,1:2:end)       , x_pos, y_pos, f(Fnum), l_size, l_size);
Map_EEP_self     = acm2skyimage(R_EEP_self(1:2:end, 1:2:end) , x_pos, y_pos, f(Fnum), l_size, l_size);
Map_EEP_holo     = acm2skyimage(R_EEP_holo                   , x_pos, y_pos, f(Fnum), l_size, l_size);
Map_EEP_stef     = acm2skyimage(R_EEP_stef(1:2:end, 1:2:end) , x_pos, y_pos, f(Fnum), l_size, l_size);

%% get differences for the angles
angles     = -angle(g_stef_eep(1:2:end)) - angle(1./G_chain(1:2:end)) +pi/2;
angle_diff = max(angles);
angle_mean = mean(angles);

angles_holo = angle(1./g_holo_eep)-2*angle(g_self_new(1))- angle(1./G_chain(1:2:end)) +pi/2;
angle_diff_h= max(angles_holo);
angle_mean_h= mean(angles_holo);

%% get differences for the gains
gains     = abs(1./g_stef_eep(1:2:end)/sqrt(norm_2)) - abs(1./G_chain(1:2:end)) ;
gain_diff = max(gains);
gain_mean = mean(gains);

g_d_holo   = abs(g_holo_eep./(norm_weight*1e3)) - abs(1./G_chain(1:2:end)) ;
g_h_diff   = max(g_d_holo);
g_h_mean   = mean(g_d_holo);
%% Figures go here

figure % skyimages after all calibration procedures
subplot(2,4,1)
imagesc(Uncalibrated_map)
colorbar
title('Uncalibrated map')
subplot(2,4,2)
imagesc(Map_self)
colorbar
title('Map self')
subplot(2,4,3)
imagesc(Map_stef)
colorbar
title('Map stef')
subplot(2,4,4)
imagesc(Map_holo)
colorbar
title('Map holo')

subplot(2,4,5)
imagesc(Uncal_EEP_map)
colorbar
title('Uncal EEP map')
subplot(2,4,6)
imagesc(Map_EEP_self)
colorbar
title('Map EEP self')
subplot(2,4,7)
imagesc(Map_EEP_stef)
colorbar
title('Map EEP stef')
subplot(2,4,8)
imagesc(Map_EEP_holo)
colorbar
title('Map EEP holo')

% figure for phase comparison and gain comparison
figure
subplot(1,2,1)
plot(1:Nant, angle(g_self_new(1:2:end)), 'bo', 1:Nant, angle(g_stef_new(1:2:end)), 'mx', 1:Nant, angle(G_holo(:,i_holo)), 'g+' );
hold on
plot(1:Nant, angle(g_self_eep(1:2:end)), 'bx', 1:Nant, angle(g_stef_eep(1:2:end)), 'm+', 1:Nant, angle(g_holo_eep), 'go');
legend('Gain Self', 'Gain Stef', 'Gain Holo', 'Gain EEP Self', 'Gain EEP Stef', 'Gain EEP Holo' )

subplot(1,2,2)
plot(1:Nant, abs(g_self_new(1:2:end)), 'bo', 1:Nant, abs(g_stef_new(1:2:end)), 'mx', 1:Nant, abs(G_holo(:,i_holo)), 'g+' );
hold on
plot(1:Nant, abs(g_self_eep(1:2:end)), 'bx', 1:Nant, abs(g_stef_eep(1:2:end)), 'm+', 1:Nant, abs(g_holo_eep), 'go');
legend('Gain Self', 'Gain Stef', 'Gain Holo', 'Gain EEP Self', 'Gain EEP Stef', 'Gain EEP Holo' )

% seperated for the paper
figure
plot(1:Nant, -angle(g_stef_new(1:2:end)), 'bo',...
     1:Nant, -angle(1./g_holo_new)- 2*angle(g_self_new(1)), 'mx',...
     1:Nant, angle(1./G_chain(1:2:end))-pi/2, 'c.', ...
     1:Nant, angle(1./g_holo_eep)-2*angle(g_self_new(1)), 'r*',...
     1:Nant, angle(g_stef_eep(1:2:end)), 'g+')
legend('Gain Stef', 'Gain Holo', 'Gain Ideal', 'Gain Holo EEP', 'Gain Stef EEP')
grid on
axis([0 256, -inf inf])
xlabel('Antenna nr [-]')
ylabel('Phase [rad]')

norm_2 = abs(G_chain(1)*R(1,1)*G_chain(1)');
norm_weight = abs(Weight(:));

figure
plot(1:Nant, abs(1./g_stef_new(1:2:end)/sqrt(norm_2)), 'bo',...
     1:Nant, abs(g_holo_new./(norm_weight*1e3)), 'mx',...
     1:Nant, abs(1./G_chain(1:2:end)), 'c.',...
     1:Nant, abs(g_holo_eep./(norm_weight*1e3)), 'r*',...
     1:Nant, abs(1./g_stef_eep(1:2:end)/sqrt(norm_2)), 'g+')
legend('Gain Stef', 'Gain Holo', 'Gain Ideal', 'Gain Holo EEP', 'Gain Stef EEP')
grid on
axis([0 256, -inf inf])
xlabel('Antenna nr [-]')
ylabel('Magnitude gain')

% get image of temperature over time
t1 = t/3600 - 18;
figure
plot(t1, TSB, '.')
hold on
plot(t1, Tamb, '.')
axis([0 24, 20 60])
grid on
xlabel('Hour of measurement')
ylabel('Temperature [\circ C]')
legend('T Smart Box','T ambient')

%% function to find the jones covariance matrix for equal eeps

function Jones_element = pol_EEP_equal(l_grid, m_grid, x_pos, y_pos, freq)

% Jones_element = pol_EEP_SKALA4AL(l, m, x_pos, y_pos)
%
% This function returns the polarized element response as Jones matrix of
% 256 SKALA4AL antennas in a proposed SKA-low configuration.
%
% Note: at the moment, the patterns at 110 MHz are returned. A discussion
% is needed about whether interpolation between the simulated spot
% frequencies is possible and, if so, how that should be done.
%
% Arguments
% l : Ndir-element vector with l-coordinates of directions of interest
% m : Ndir-element vector with m-coordinates of directions of interest
% x_pos : x_position of antennas for specified station in SKA_LOW_statbeam
% y_pos : y_position of antennas for specified station in SKA_LOW_statbeam
%
% Return value
% J : 2-by-2-by-256-Ndir tensor containing 2-by-2 Jones matrices for each
%     of the 256 elements for the specified directions
%
% Stefan J. Wijnholds, 10 August 2020
% Rewritten for personal clarity 5-8-2020 Ivar updated for equal eeps
%% Variables
% load SKALA4AL EEPs
load /home/jansen/Desktop/Calibration_code/Receive_path_gain/EEP_SKALA4AL_110MHz_38m.mat Ephi Eth theta phi

% define grid used during simulations

Ntheta   = length(theta); % length of theta
Nphi     = length(phi);   % length of phi
Nant     = size(Ephi,3);  % number of elements
%freq     = 110e6;         % frequency of EM simulation in Hz
c        = 2.99792e8;     % speed of light in m/s
Npol     = size(Ephi,2);
% convert to radian
theta = deg2rad(theta);
phi   = deg2rad(phi)  ;

%% Convert to single EEP for all 

Ephi_EEP_mag = sum(abs(Ephi),3)/Nant; % take sum and scale
Eth_EEP_mag  = sum(abs(Eth), 3)/Nant;

% allocate memory for speed
Ephi_EEP = zeros(1,Npol, Nant, Ntheta*Nphi);
Eth_EEP  = zeros(1,Npol, Nant, Ntheta*Nphi);

% apply phase vector back again
for i = 1:Nant
    Ephi_EEP(1,:,i,:) = Ephi_EEP_mag.*exp(1i*angle(Ephi(1,:,i,:)));
    Eth_EEP( 1,:,i,:) = Eth_EEP_mag.*exp(1i*angle(Eth(  1,:,i,:)));
end

%%
% augment vector of phi to avoid interpolation issues
phi = [2 * phi(1) - phi(2), phi, 2 * phi(end) - phi(end-1)];

% define grid for delay correction
[phigrid, thetagrid] = meshgrid(phi, theta);

% determine gains for specified directions
Ndir   = length(l_grid);                % calc Datapoints
lmdist = sqrt(l_grid.^2 + m_grid.^2);   % get grid of distances from origin

% convert (l,m) to (theta, phi)
phi0   = atan2(l_grid(lmdist <= 1), m_grid(lmdist <= 1)) + pi; % calc phi   back from lm grid within sphere
theta0 = asin(lmdist(lmdist <= 1));                            % calc theta back from lm grid within sphere

% pre-allocation
Jones_element = zeros(2, size(Ephi,2), size(Ephi,3), Ndir);
Eph           = zeros(Ntheta, Nphi+2);
Etheta        = zeros(Ntheta, Nphi+2);

for i = 1:Nant % for each antenna
    
    Jones_element(:, :, i, lmdist > 1) = NaN; % exclude points outside viewing sphere/unit circle
    
    % calculate delay correction for (phi, theta)-grid and for (l, m)-grid
    delaycor_thphi = exp((-2*pi*1j*freq/c) * (x_pos(i)*cos(phigrid).*sin(thetagrid) + y_pos(i)*sin(phigrid).*sin(thetagrid)));
    delaycor_lm    = exp((-2*pi*1j*freq/c) * (x_pos(i) * l_grid(lmdist <= 1) + y_pos(i) * m_grid(lmdist <= 1)));
    
    for pol = 1:2 % for each polarization
        % collect data on Ephi and Etheta, for short hand notation
        Eph(:, 2:end-1) = reshape(Ephi_EEP(1, pol,i, :), [Ntheta, Nphi]); % Eph [Ntheta x Nphi]
        Eph(:, 1)       = Eph(:, end-1);                               % ensure proper enclosure
        Eph(:, end)     = Eph(:, 2);                                   % ensure proper enclosure
        
        Etheta(:, 2:end-1) = reshape(Eth_EEP(1, pol,i, :), [Ntheta, Nphi]);  % Eth [Ntheta x Nphi]
        Etheta(:, 1)       = Etheta(:, end-1);                               % ensure proper enclosure
        Etheta(:, end)     = Etheta(:, 2);                                   % ensure proper enclosure
        
        % The following lines perform three operations
        % 1. move the phase reference to the element being considered;
        % 2. regridding from (theta, phi)-grid to (l, m)-grid by
        % 3. move the phase reference back to common phase reference
        
        Jones_element(pol, 1, i, lmdist <= 1) = interp2(phi, theta, Eph    .* delaycor_thphi, phi0, theta0) .* conj(delaycor_lm);
        Jones_element(pol, 2, i, lmdist <= 1) = interp2(phi, theta, Etheta .* delaycor_thphi, phi0, theta0) .* conj(delaycor_lm);
    end
end
end


