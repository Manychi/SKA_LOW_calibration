function Jones_element = pol_EEP_SKALA4AL(l_grid, m_grid, x_pos, y_pos, freq)

% Jones_element = pol_EEP_SKALA4AL(l, m, x_pos, y_pos)
%
% This function returns the polarized element response as Jones matrix of
% 256 SKALA4AL antennas in a proposed SKA-low configuration.
%
% Note: at the moment, the patterns at 110 MHz are returned. A discussion
% is needed about whether interpolation between the simulated spot
% frequencies is possible and, if so, how that should be done.
%
% Arguments
% l : Ndir-element vector with l-coordinates of directions of interest
% m : Ndir-element vector with m-coordinates of directions of interest
% x_pos : x_position of antennas for specified station in SKA_LOW_statbeam
% y_pos : y_position of antennas for specified station in SKA_LOW_statbeam
%
% Return value
% J : 2-by-2-by-256-Ndir tensor containing 2-by-2 Jones matrices for each
%     of the 256 elements for the specified directions
%
% Stefan J. Wijnholds, 10 August 2020
% Rewritten for personal clarity 5-8-2020 Ivar
%% Variables
% load SKALA4AL EEPs
load /home/jansen/Desktop/Calibration_code/Receive_path_gain/EEP_SKALA4AL_110MHz_38m.mat Ephi Eth theta phi

% define grid used during simulations

Ntheta   = length(theta); % length of theta
Nphi     = length(phi);   % length of phi
Nant     = size(Ephi,3);  % number of elements
%freq     = 110e6;         % frequency of EM simulation in Hz
c        = 2.99792e8;     % speed of light in m/s

% convert to radian
theta = deg2rad(theta);
phi   = deg2rad(phi)  ;

%%
% augment vector of phi to avoid interpolation issues
phi = [2 * phi(1) - phi(2), phi, 2 * phi(end) - phi(end-1)];

% define grid for delay correction
[phigrid, thetagrid] = meshgrid(phi, theta);

% determine gains for specified directions
Ndir   = length(l_grid);                % calc Datapoints
lmdist = sqrt(l_grid.^2 + m_grid.^2);   % get grid of distances from origin

% convert (l,m) to (theta, phi)
phi0   = atan2(l_grid(lmdist <= 1), m_grid(lmdist <= 1)) + pi; % calc phi   back from lm grid within sphere
theta0 = asin(lmdist(lmdist <= 1));                            % calc theta back from lm grid within sphere

% pre-allocation
Jones_element = zeros(2, size(Ephi,2), size(Ephi,3), Ndir);
Eph           = zeros(Ntheta, Nphi+2);
Etheta        = zeros(Ntheta, Nphi+2);

for i = 1:Nant % for each antenna
    
    Jones_element(:, :, i, lmdist > 1) = NaN; % exclude points outside viewing sphere/unit circle
    
    % calculate delay correction for (phi, theta)-grid and for (l, m)-grid
    delaycor_thphi = exp((-2*pi*1j*freq/c) * (x_pos(i)*cos(phigrid).*sin(thetagrid) + y_pos(i)*sin(phigrid).*sin(thetagrid)));
    delaycor_lm    = exp((-2*pi*1j*freq/c) * (x_pos(i) * l_grid(lmdist <= 1) + y_pos(i) * m_grid(lmdist <= 1)));
    
    for pol = 1:2 % for each polarization
        % collect data on Ephi and Etheta, for short hand notation
        Eph(:, 2:end-1) = reshape(Ephi(1, pol, i, :), [Ntheta, Nphi]); % Eph [Ntheta x Nphi]
        Eph(:, 1)       = Eph(:, end-1);                               % ensure proper enclosure
        Eph(:, end)     = Eph(:, 2);                                   % ensure proper enclosure
        
        Etheta(:, 2:end-1) = reshape(Eth(1, pol, i, :), [Ntheta, Nphi]);  % Eth [Ntheta x Nphi]
        Etheta(:, 1)       = Etheta(:, end-1);                               % ensure proper enclosure
        Etheta(:, end)     = Etheta(:, 2);                                   % ensure proper enclosure
        
        % The following lines perform three operations
        % 1. move the phase reference to the element being considered;
        % 2. regridding from (theta, phi)-grid to (l, m)-grid by
        % 3. move the phase reference back to common phase reference
        
        Jones_element(pol, 1, i, lmdist <= 1) = interp2(phi, theta, Eph    .* delaycor_thphi, phi0, theta0) .* conj(delaycor_lm);
        Jones_element(pol, 2, i, lmdist <= 1) = interp2(phi, theta, Etheta .* delaycor_thphi, phi0, theta0) .* conj(delaycor_lm);
    end
end

