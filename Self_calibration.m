function [Gains,i] = Self_calibration(M,R,error,imax)
%% Self_calibration
% This script does the full StEFCal calibration in order to enhance the
% figure.
%
% arguments:
%     M                   : Model corarience matrix
%     R                   : The measured covarience matrix.
%     Nant                : The weights introduced by BF, MVDR or AAR. 
%     error               : Minimal differentiation between iterations 
%     imax                : Max amount of iterations
%     
% returns:
%     Gain_holo           : The new estimated gain after calibration
%     i                   : Number of iterations 
%            
%% Initialization
Nant      = size(R,1);      % find amount of antennas
Gains     = ones(Nant,imax+1);% Initial guess as ones on the diagonal
time      = 1;

%% Cycle for calibration  

for i = 1:imax                              % for i till max iterations
    for j = 1:Nant                          % for all antennas
        z = Gains(:,i).*M(:,j);        % local vector for new gain per antenna
        Gains(j,i+1) = (R(:,j)'*z)/(z'*z);   % Calc new gain
    end
            
    if mod(i,2) == 0                        % for every 2nd iteration
        
        if norm(Gains(:,i+1) - Gains(:,i))/norm(Gains(:,i+1))<error
            disp(['Self Calibration convergence reached after ' num2str(i) ' iterations']);
            break;            
        
        else
            Gains(:,i+1) = (Gains(:,i+1) + Gains(:,i))/2;% average to not spiral out of control
        end
    end
end