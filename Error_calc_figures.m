%% Error_calc_figures
%
% This script is meant to give meaning to the values created in Error_calc.
% The plot(1) is gain in V/V and phase over frequency. Plot(2) is gain in
% dB and phase over frequency, i can be chosen between 1:61 (50-350MHz).
% Plot(3) is a demonstration of the mean value over the last 5min for each
% antenna over frequency. This is to demonstrate the difference in gain,
% whilst showing similar frequency behaviour, and their impact on the total
% gain.
% Plot(4) is the maximum error total, and LNA,FEM & total, as FO, and
% preADU significantly less impact, see plot(3). 
% Plot(5) shows the behaviour of the first antenna phase, at a certain
% frequency over time, for each module+total. 
% Plot(6) shows the behaviour of each antenna at 50MHz over time for each
% module + total.
% Plot(7) shows the behaviour of the fibre optic over time per frequency in
% terms of maximum and minimum.
% Plot(8) shows the phase behaviour per antenna over frequency and time.
% Plot(9) shows the phase max/mean/min for each module per frequency over
% time.
% Plot(10) shows the maximum possible errors in terms of degree deviation
% per module + total and the maximum possible errors of each module
% combined.
% Plot(11) is figure(2) but over time instead of frequency, i being chosen
% between 1-1440min i.e. 24hours.
% Plot(12) shows the difference between antennas at a certain frequency and
% time as a histogram, with a gaussian plotted over the histogram. 
% Plot(13) shows the phase std over frequency of all antennas 
% Plot(14) shows the phase std over time of all antennas
% Plot(15) shows the gain std over frequency of all antennas
% Plot(16) shows the gain std over time of all antennas
% Figure(88) is a test figure showing the LNA phase and gain behaviour 
% averaged over 5mins of antenna 256  

%% All actual figures:
figure(1) % figure showing errors/deviation in magnitude and rad 

for i = 1:2
  
    subplot(2,2,1)
    errorbar(1:Nf, mean_lin_LNAx(:,i),mean_lin_LNAx(:,i)-min_lin_LNAx(:,i) , max_lin_LNAx(:,i)-mean_lin_LNAx(:,i))
    hold on
    errorbar(1:Nf, mean_lin_FEMx(:,i),mean_lin_FEMx(:,i)-min_lin_FEMx(:,i) , max_lin_FEMx(:,i)-mean_lin_FEMx(:,i))
    errorbar(1:Nf, mean_lin_FOx(:,i) ,mean_lin_FOx(:,i)-min_lin_FOx(:,i)  , max_lin_FOx(:,i)-mean_lin_FOx(:,i))
    errorbar(1:Nf, mean_lin_preADUx(:,i),mean_lin_preADUx(:,i)-min_lin_preADUx(:,i) , max_lin_preADUx(:,i)-mean_lin_preADUx(:,i))
    hold off
    xlabel('Timestamp nr [-]')
    legend('LNA','FEM','FO','preADU')
    title(['|Gain| deviation xpol at each timestamp' num2str(i) ' for each frequency' ])
    
    subplot(2,2,2)
    errorbar(1:Nf, mean_lin_totx(:,i), mean_lin_totx(:,i)-min_lin_totx(:,i), max_lin_totx(:,i)-mean_lin_totx(:,i))
    ylabel('Amp gain')
    
    subplot(2,2,3)
    errorbar(1:Nf, mean_ph_LNAx(:,i),mean_ph_LNAx(:,i)-min_ph_LNAx(:,i) , max_ph_LNAx(:,i)-mean_ph_LNAx(:,i))
    hold on
    errorbar(1:Nf, mean_ph_FEMx(:,i),mean_ph_FEMx(:,i)-min_ph_FEMx(:,i) , max_ph_FEMx(:,i)-mean_ph_FEMx(:,i))
    errorbar(1:Nf, mean_ph_FOx(:,i) ,mean_ph_FOx(:,i)-min_ph_FOx(:,i)  , max_ph_FOx(:,i)-mean_ph_FOx(:,i))    
    errorbar(1:Nf, mean_ph_preADUx(:,i),mean_ph_preADUx(:,i)-min_ph_preADUx(:,i) , max_ph_preADUx(:,i)-mean_ph_preADUx(:,i))
    hold off
    xlabel('Timestamp nr [-]')
    legend('LNA','FEM','FO','preADU')
    title('phase deviation xplot')
    
    subplot(2,2,4)
    errorbar(1:Nf, mean_ph_totx(:,i),mean_ph_totx(:,i)-min_ph_totx(:,i) , max_ph_totx(:,i)-mean_ph_totx(:,i))
    ylabel('Amp gain')
    pause(0.05)
    
end
%%
figure(2) % figure showing error/deviation in db and rad 

for i = 1:2 % for every timestep
    
    subplot(2,2,1)
    errorbar(1:Nf, 20*log10(mean_lin_LNAx(:,i)),20*log10(mean_lin_LNAx(:,i))-20*log10(min_lin_LNAx(:,i)) ...
                 , 20*log10(max_lin_LNAx(:,i))-20*log10(mean_lin_LNAx(:,i)))
    hold on
    errorbar(1:Nf, 20*log10(mean_lin_FEMx(:,i)),20*log10(mean_lin_FEMx(:,i))-20*log10(min_lin_FEMx(:,i)) ...
                 , 20*log10(max_lin_FEMx(:,i))-20*log10(mean_lin_FEMx(:,i)))
    errorbar(1:Nf, 20*log10(mean_lin_FOx(:,i)) ,20*log10(mean_lin_FOx(:,i))-20*log10(min_lin_FOx(:,i)) ...
                 , 20*log10(max_lin_FOx(:,i))-20*log10(mean_lin_FOx(:,i)))
    errorbar(1:Nf, 20*log10(mean_lin_preADUx(:,i)),20*log10(mean_lin_preADUx(:,i))-20*log10(min_lin_preADUx(:,i)) ...
                 , 20*log10(max_lin_preADUx(:,i))-20*log10(mean_lin_preADUx(:,i)))
    hold off
    xlabel('Frequency [MHz]')
    ylabel('Amp gain dB')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    legend('LNA','FEM','FO','preADU')
    %title(['dB(|Gain|) deviation xpol at timestamp' num2str(i) ])
    
    subplot(2,2,2)
    errorbar(1:Nf, 20*log10(mean_lin_totx(:,i)), mag2db(mean_lin_totx(:,i))-20*log10(min_lin_totx(:,i))...
                 , 20*log10(max_lin_totx(:,i))-mag2db(mean_lin_totx(:,i)))
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    %title('Total signal chain dB(|Gain|) devation xpol')
    
    subplot(2,2,3)
    errorbar(1:Nf, (mean_ph_LNAx(:,i)),(mean_ph_LNAx(:,i)-min_ph_LNAx(:,i)) , (max_ph_LNAx(:,i)-mean_ph_LNAx(:,i)))
    hold on
    errorbar(1:Nf, (mean_ph_FEMx(:,i)),(mean_ph_FEMx(:,i)-min_ph_FEMx(:,i)) , (max_ph_FEMx(:,i)-mean_ph_FEMx(:,i)))
    errorbar(1:Nf, (mean_ph_FOx(:,i)),(mean_ph_FOx(:,i)-min_ph_FOx(:,i)) , (max_ph_FOx(:,i)-mean_ph_FOx(:,i)))  
    errorbar(1:Nf, (mean_ph_preADUx(:,i)),(mean_ph_preADUx(:,i)-min_ph_preADUx(:,i)) , (max_ph_preADUx(:,i))-mean_ph_preADUx(:,i))
    hold off
    xlabel('Frequency  [MHz]')
    ylabel('\angle devation [rad]')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)

    legend('LNA','FEM','FO','preADU')
    %title(['\angle deviation xpol at timestamp' num2str(i)])
    
    subplot(2,2,4)
    errorbar(1:Nf, mean_ph_totx(:,i),mean_ph_totx(:,i)-min_ph_totx(:,i) ,max_ph_totx(:,i)-mean_ph_totx(:,i))
    ylabel('\angle deviation [rad]')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    %title('Total signal chain \angle devation xpol')
    pause(0.05)
end


%%
figure(3) 
subplot(1,5,1)
plot(1:Nf, 20*log10(Means_ant(:,:,1)))
axis([0 Nf, 40 50])
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain [dB]')

subplot(1,5,2)
plot(1:Nf, 20*log10(Means_ant(:,:,2)))
axis([0 Nf, 55 65])
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain [dB]')

subplot(1,5,3)
plot(1:Nf, 20*log10(Means_ant(:,:,3)))
axis([0 Nf, -2 2])
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain [dB]')

subplot(1,5,4)
plot(1:Nf, 20*log10(Means_ant(:,:,4)))
axis([0 Nf, -2 2])
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain [dB]')

subplot(1,5,5)
plot(1:Nf, 20*log10(Means_ant(:,:,5)))
axis([0 Nf, 95 110])
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain [dB]')

%%
for i = 1:Nf
figure(4) 
subplot(1,2,1)
plot(1:Nt/5, 100*squeeze(Max_error_stage(6,i,:)))
legend('Max possible error')
xlabel('Hour of the day ')
ylabel('Error in %')
title(['Maximum possible error frequency: ' num2str(f(i)/1e6) 'MHz'])
axis([0 Nt/5, 12 13.5])
set(gca,'XTick',0:24:Nt/5)
set(gca,'XTickLabel',0:2:24) 

subplot(1,2,2)
plot(1:Nt/5, 100*squeeze(Max_error_stage(1,i,:)))
hold on
plot(1:Nt/5, 100*squeeze(Max_error_stage(2,i,:)))
plot(1:Nt/5, 100*squeeze(Max_error_stage(5,i,:)))
hold off
axis([0 Nt/5, 5 8])
legend('LNA max error','FEM max error','tot max error')
title(['Max error all antennas frequency: ' num2str(f(i)/1e6) 'MHz'])
set(gca,'XTick',0:24:Nt/5)
set(gca,'XTickLabel',0:2:24) 
pause(0.05)
end

%%
figure(5)
for i = 1:Nf
    plot(1:Nt, squeeze(full_angle_avg(1,i,:,:)))
    title(['Average phase over 5mins for each module at freq: ' num2str(f(i)/1e6) ' MHz ' ])
    legend('LNA','FEM','FO','preADU','tot')
    pause(0.1)
end
%%
figure(6)
% Shows difference between antennas, marginal differences are observed
for i = 1:Nant
    plot(1:Nt, squeeze(full_angle_avg(i,1,:,:)))
    title(['Average phase over 5mins for each module at antenna: ' num2str(i)])
    legend('LNA','FEM','FO','preADU','tot')
    pause(0.1)
end

%%
figure(7)
for i = 1:Nf
    plot(1:Nt, squeeze(full_angle_max(i,:,3)))
    hold on
    plot(1:Nt, squeeze(full_angle_min(i,:,3)))
    hold off
    % legend('LNA','FEM','FO','preADU','tot')
    title(['Fibre Optic phase change for frequency: ' num2str(f(i)/1e6) 'MHz'] )
    pause(0.1)
end


%%
figure(8)
for i = 1:Nant
   
    subplot(1,5,1)
    imagesc(transpose(squeeze(Ph_mean(i,:,:,1))))
    colorbar
    title('LNA')
    xlabel('Frequency MHz')
    ylabel('Hour of measurement')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    set(gca,'YTick',0:24:Nt/5)
    set(gca,'YTickLabel',0:2:24) 
    
    subplot(1,5,2)
    imagesc(transpose(squeeze(Ph_mean(i,:,:,2))))
    colorbar
    title('FEM')
    xlabel('Frequency MHz')
    ylabel('Hour of measurement')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    set(gca,'YTick',0:24:Nt/5)
    set(gca,'YTickLabel',0:2:24)  
    
    subplot(1,5,3)
    imagesc(transpose(squeeze(Ph_mean(i,:,:,3))))
    colorbar
    title('FO')
    xlabel('Frequency MHz')
    ylabel('Hour of measurement')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    set(gca,'YTick',0:24:Nt/5)
    set(gca,'YTickLabel',0:2:24)  
    
    subplot(1,5,4)
    imagesc(transpose(squeeze(Ph_mean(i,:,:,4))))
    colorbar
    title('preADU')
    xlabel('Frequency MHz')
    ylabel('Hour of measurement')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    set(gca,'YTick',0:24:Nt/5)
    set(gca,'YTickLabel',0:2:24) 
    
    subplot(1,5,5)
    imagesc(transpose(squeeze(Ph_mean(i,:,:,5))))
    colorbar
    title('total')
    xlabel('Frequency MHz')
    ylabel('Hour of measurement')
    set(gca,'XTick',0:10:61)
    set(gca,'XTickLabel',50:50:350)
    set(gca,'YTick',0:24:Nt/5)
    set(gca,'YTickLabel',0:2:24) 
    sgtitle(['Phase mean over 5 mins, receive path phases for antenna: ' num2str(i)])
pause(0.05)
end
%%
for i = 1:Nf
    figure(9)
    subplot(1,5,1)
    plot(1:Nt, squeeze(full_angle_max(i,:,1)))
    hold on
    plot(1:Nt, squeeze(full_angle_mean(i,:,1)))
    plot(1:Nt, squeeze(full_angle_min(i,:,1)))
    hold off
    title(['Phase error LNA for ' num2str(f(i)/1e6) ' MHz'])
    legend('max','mean','min')
    xlabel('Hour of the day ')    
    axis([0 Nt, -360 1080])
    set(gca,'XTick',0:120:Nt)
    set(gca,'XTickLabel',0:2:24)
    
    subplot(1,5,2)
    plot(1:Nt, squeeze(full_angle_max(i,:,2)))
    hold on
    plot(1:Nt, squeeze(full_angle_mean(i,:,2)))
    plot(1:Nt, squeeze(full_angle_min(i,:,2)))
    hold off
    title(['Phase error FEM for ' num2str(f(i)/1e6) ' MHz'])
    legend('max','mean','min')
    xlabel('Hour of the day ')
    axis([0 Nt, -360 1080])
    set(gca,'XTick',0:120:Nt)
    set(gca,'XTickLabel',0:2:24)
    
    subplot(1,5,3)
    plot(1:Nt, squeeze(full_angle_max(i,:,3)))
    hold on
    plot(1:Nt, squeeze(full_angle_mean(i,:,3)))
    plot(1:Nt, squeeze(full_angle_min(i,:,3)))
    hold off
    title(['Phase error FO for ' num2str(f(i)/1e6) ' MHz'])
    legend('max','mean','min')
    xlabel('Hour of the day ')  
    axis([0 Nt, -360 1080])
    set(gca,'XTick',0:120:Nt)
    set(gca,'XTickLabel',0:2:24)
    
    subplot(1,5,4)
    plot(1:Nt, squeeze(full_angle_max(i,:,4)))
    hold on
    plot(1:Nt, squeeze(full_angle_mean(i,:,4)))
    plot(1:Nt, squeeze(full_angle_min(i,:,4)))
    hold off
    title(['Phase error preADU for ' num2str(f(i)/1e6) ' MHz'])
    legend('max','mean','min')
    xlabel('Hour of the day ')   
    axis([0 Nt, -360 1080])
    set(gca,'XTick',0:120:Nt)
    set(gca,'XTickLabel',0:2:24)
    
    subplot(1,5,5)
    plot(1:Nt, squeeze(full_angle_max(i,:,5)))
    hold on
    plot(1:Nt, squeeze(full_angle_mean(i,:,5)))
    plot(1:Nt, squeeze(full_angle_min(i,:,5)))
    hold off
    title(['Phase error total for ' num2str(f(i)/1e6) ' MHz'])
    legend('max','mean','min')
    xlabel('Hour of the day ') 
    axis([0 Nt, -360 1080])
    set(gca,'XTick',0:120:Nt)
    set(gca,'XTickLabel',0:2:24)
    
    pause(0.5)
end
%%
for i = 1:Nf
figure(10) 
subplot(1,2,1)
plot(1:Nt, 100*squeeze(max_phase_error(5,i,:)))
legend('Max possible error')
xlabel('Hour of the day ')

axis([0 Nt, -inf inf])
set(gca,'XTick',0:24:Nt/5)
set(gca,'XTickLabel',0:2:24) 
ylabel('Error in %')
title(['Maximum possible error frequency: ' num2str(f(i)/1e6) 'MHz'])
subplot(1,2,2)
plot(1:Nt, squeeze(max_phase_error(1,i,:)))
hold on
plot(1:Nt, squeeze(max_phase_error(2,i,:)))
plot(1:Nt, squeeze(max_phase_error(3,i,:)))
plot(1:Nt, squeeze(max_phase_error(4,i,:)))
plot(1:Nt, squeeze(max_phase_error(5,i,:)))
hold off
axis([0 Nt, 0 100])
legend('LNA max error','FEM max error','FO max error','preADU max error','tot max error')
title(['Max error all antennas frequency: ' num2str(f(i)/1e6) 'MHz'])
set(gca,'XTick',0:24:Nt/5)
set(gca,'XTickLabel',0:2:24) 
pause(0.05)
end

%%
figure(11)

for i = 1:Nf % for every frequency
    
    subplot(2,2,1)
    errorbar(1:10:Nt, 20*log10(mean_lin_LNAx(i,1:10:Nt)),20*log10(mean_lin_LNAx(i,1:10:Nt))-20*log10(min_lin_LNAx(i,1:10:Nt)) ...
                 , 20*log10(max_lin_LNAx(i,1:10:Nt))-20*log10(mean_lin_LNAx(i,1:10:Nt)))
    hold on
    errorbar(1:10:Nt, 20*log10(mean_lin_FEMx(i,1:10:Nt)),20*log10(mean_lin_FEMx(i,1:10:Nt))-20*log10(min_lin_FEMx(i,1:10:Nt)) ...
                 , 20*log10(max_lin_FEMx(i,1:10:Nt))-20*log10(mean_lin_FEMx(i,1:10:Nt)))
    errorbar(1:10:Nt, 20*log10(mean_lin_FOx(i,1:10:Nt)) ,20*log10(mean_lin_FOx(i,1:10:Nt))-20*log10(min_lin_FOx(i,1:10:Nt)) ...
                 , 20*log10(max_lin_FOx(i,1:10:Nt))-20*log10(mean_lin_FOx(i,1:10:Nt)))
    errorbar(1:10:Nt, 20*log10(mean_lin_preADUx(i,1:10:Nt)),20*log10(mean_lin_preADUx(i,1:10:Nt))-20*log10(min_lin_preADUx(i,1:10:Nt)) ...
                 , 20*log10(max_lin_preADUx(i,1:10:Nt))-20*log10(mean_lin_preADUx(i,1:10:Nt)))
    hold off
    axis([0 1440, -inf inf])
    xlabel('Hour of measurement')
    ylabel('Amp gain [dB]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    legend('LNA','FEM','FO','preADU')
    title(['dB(|Gain|) deviation xpol at timestamp' num2str(i) ])
    
    subplot(2,2,2)
    errorbar(1:10:Nt, 20*log10(mean_lin_totx(i,1:10:Nt)), mag2db(mean_lin_totx(i,1:10:Nt))-20*log10(min_lin_totx(i,1:10:Nt))...
                 , 20*log10(max_lin_totx(i,1:10:Nt))-mag2db(mean_lin_totx(i,1:10:Nt)))
    axis([0 1440, -inf inf])
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    title('Total signal chain dB(|Gain|) devation xpol')
    
    subplot(2,2,3)
    errorbar(1:10:Nt, (mean_ph_LNAx(i,1:10:Nt)),(mean_ph_LNAx(i,1:10:Nt)-min_ph_LNAx(i,1:10:Nt)) , ...
                      (max_ph_LNAx(i,1:10:Nt)-mean_ph_LNAx(i,1:10:Nt)))
    hold on
    errorbar(1:10:Nt, (mean_ph_FEMx(i,1:10:Nt)),(mean_ph_FEMx(i,1:10:Nt)-min_ph_FEMx(i,1:10:Nt)) , ...
                      (max_ph_FEMx(i,1:10:Nt)-mean_ph_FEMx(i,1:10:Nt)))
    errorbar(1:10:Nt, (mean_ph_FOx(i,1:10:Nt)),(mean_ph_FOx(i,1:10:Nt)-min_ph_FOx(i,1:10:Nt)) , ...
                      (max_ph_FOx(i,1:10:Nt)-mean_ph_FOx(i,1:10:Nt)))  
    errorbar(1:10:Nt, (mean_ph_preADUx(i,1:10:Nt)),(mean_ph_preADUx(i,1:10:Nt)-min_ph_preADUx(i,1:10:Nt)) ,...
                   (max_ph_preADUx(i,1:10:Nt))-mean_ph_preADUx(i,1:10:Nt))
    hold off
    axis([0 1440, -inf inf])
    xlabel('Hour of measurement')
    ylabel('\angle devation [rad]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    legend('LNA','FEM','FO','preADU')
    title(['\angle deviation xpol at timestamp' num2str(i)])
    
    subplot(2,2,4)
    errorbar(1:10:Nt, mean_ph_totx(i,1:10:Nt),mean_ph_totx(i,1:10:Nt)-min_ph_totx(i,1:10:Nt) ,...
                      max_ph_totx(i,1:10:Nt)-mean_ph_totx(i,1:10:Nt))
    axis([0 1440, -inf inf])
    ylabel('\angle deviation [rad]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    title('Total signal chain \angle devation xpol')
    pause(0.05)
end

%%
for i = 1:Nf
figure(12)
histogram(mag2db(squeeze(abs(g_totx(:,i,1)))))
histfit(mag2db(squeeze(abs(g_totx(:,i,1)))))

xlabel('Total gain magnitude [dB]')
ylabel('Nr antennas between gain interval [-]')
axis([97.5 110, 0 45])
pause(0.5)
end


%%
for i = 1:Nf
    figure(13)
    subplot(1,5,1)
    plot(squeeze(Phase_std(i,:,1))) 
    ylabel(['\angle standard deviation' char(176) ])
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    subplot(1,5,2)
    plot(squeeze(Phase_std(i,:,2)))   
    ylabel('\angle standard deviation ')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)    
    subplot(1,5,3)    
    plot(squeeze(Phase_std(i,:,3)))
    ylabel('\angle std ')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)    
    subplot(1,5,4)    
    plot(squeeze(Phase_std(i,:,4)))
    ylabel('\angle deviation ')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)    
    subplot(1,5,5)    
    plot(squeeze(Phase_std(i,:,5)))
    ylabel('\angle deviation ')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    pause(0.5)
end
%%

figure(14)
subplot(1,5,1)
plot(squeeze(Phase_std(:,1,1)))
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Phase \angle')
subplot(1,5,2)
plot(squeeze(Phase_std(:,1,2)))
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Phase \angle')
subplot(1,5,3)
plot(squeeze(Phase_std(:,1,3)))
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Phase \angle')
subplot(1,5,4)
plot(squeeze(Phase_std(:,1,4)))
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Phase \angle')
subplot(1,5,5)
plot(squeeze(Phase_std(:,1,5)))
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Phase \angle')


%%
for i = 1:Nf
    figure(15)
    subplot(1,5,1)
    plot(squeeze(Gain_std(i,:,1)))
    ylabel('\angle deviation [rad]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    
    subplot(1,5,2)
    plot(squeeze(Gain_std(i,:,2)))
    ylabel('\angle deviation [rad]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    
    subplot(1,5,3)
    plot(squeeze(Gain_std(i,:,3)))
    ylabel('\angle deviation [rad]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    
    subplot(1,5,4)
    plot(squeeze(Gain_std(i,:,4)))
    ylabel('\angle deviation [rad]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    
    subplot(1,5,5)
    plot(squeeze(Gain_std(i,:,5)))
    ylabel('\angle deviation [rad]')
    set(gca,'XTick',0:60:1440)
    set(gca,'XTickLabel',0:1:24)
    pause(0.5)
end

%%
figure(16)
subplot(1,5,1)
plot(squeeze(Gain_std(:,1,1)))
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
ylabel('Gain std [dB]')
title('Gain std LNA')
subplot(1,5,2)
plot(squeeze(Gain_std(:,1,2)))
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
title('Gain std FEM')
subplot(1,5,3)
plot(squeeze(Gain_std(:,1,3)))
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
title('Gain std FO')
subplot(1,5,4)
plot(squeeze(Gain_std(:,1,4)))
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
title('Gain std preADU')
subplot(1,5,5)
plot(squeeze(Gain_std(:,1,5)))
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
xlabel('Frequency [MHz]')
title('Gain std tot')
%% change of antenna 1
mean_lin = zeros(Nt/5, Nf);
phase_lin = zeros(Nt/5,Nf);
ant = 256;
for i = 1:Nt/5
    for j = 1:Nf
        mean_lin(i,j) = mean(abs(gLNAx(ant,j,(i-1)*5+1:5*i)));
        phase_lin(i,j) = mean(angle(gLNAx(ant,j,(i-1)*5 + 1:5*i)));
    end
end

figure(88)
subplot(1,2,1)
imagesc(mean_lin)
colorbar
xlabel('Frequency [MHz]')
ylabel('Every 5th timestep')
title('Gain LNA')
set(gca,'XTick',0:5:61)
set(gca,'XTickLabel',50:25:350)
set(gca,'YTick',0:24:Nt/5)
set(gca,'YTickLabel',0:2:24) 

subplot(1,2,2)
imagesc(phase_lin)
colorbar
xlabel('Frequency MHz')
ylabel('Every 5th timestep')
title('Phase LNA [rad]')
set(gca,'XTick',0:10:61)
set(gca,'XTickLabel',50:50:350)
set(gca,'YTick',0:24:Nt/5)
set(gca,'YTickLabel',0:2:24) 

